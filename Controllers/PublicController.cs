﻿using System.Runtime.Caching;
using System.Text;
using AttributeRouting.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SI.BLL;
using SI.DTO;
using SI.Services.Public;
using SI.WEB.Portal.Framework;
using SI.WEB.Portal.Json;
using SI.WEB.Portal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace SI.WEB.Portal.Controllers
{
    public class PublicController : SuperController
    {
        [Route("/", RouteName = "Home")]
        [CompressResponse]
        public ActionResult Index()
        {
            BaseModel model = new BaseModel();
            ViewBag.BaseModel = model;

            if (Request.IsAuthenticated)
                Response.Redirect(Url.RouteUrl("DashboardOverview"), true);
            else
                Response.Redirect("/Account/Login", true);

            return View();
        }

        #region SSI get token and signin


        [HttpGet]
        [Route("/ssosignin/{accesstoken}")]
        public JsonResult SSOSignin(Guid? accesstoken)
        {
            if (accesstoken != null && accesstoken.HasValue)
            {
                if (PublicService.ValidateSSI(accesstoken.GetValueOrDefault(), Request.Url.Authority, Request.UserAgent))
                {
                    Response.Redirect(Url.RouteUrl("DashboardOverview"), true);
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }

            Request.Abort();
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Route("/createaccesstoken")]
        public JsonResult CreateAccessToken(SSOTokenRequest req)
        {
            if (req != null) // can be null if no binding
            {
                Guid accessToken = PublicService.CreateAccessToken(req.ResellerKey, req.UserKey);

                if (accessToken != Guid.Empty)
                    return Json(new SSOTokenResponse() { AccessToken = accessToken });
            }
            Request.Abort();
            return Json("");
        }


        [HttpGet]
        [Route("/access/{accesstoken}", RouteName = "ExternalAccess")]
        public JsonResult ExternalAccess(Guid? accesstoken)
        {
            if (accesstoken != null && accesstoken.HasValue)
            {
                AccessTokenBaseDTO accessTokenBaseDto = ProviderFactory.Security.GetAccessToken(accesstoken.Value, true);

                if (accessTokenBaseDto != null)
                {
                    if (accessTokenBaseDto.TimeTillExpiration != null)
                    {
                        if (accessTokenBaseDto.TimeTillExpiration.GetValueOrDefault().CompareTo(TimeSpan.Zero) < 0)
                            return Json("Token has Expired", JsonRequestBehavior.AllowGet);
                    }

                    if (accessTokenBaseDto.RemainingUses != null)
                    {
                        if (accessTokenBaseDto.RemainingUses.GetValueOrDefault(1) <= 0)
                            return Json("Token has Expired", JsonRequestBehavior.AllowGet);
                    }

                    ValidateUserResultDTO validateUserResultDto = ProviderFactory.Security.ValidateUser(new UserInfoDTO(accessTokenBaseDto.UserID, null));

                    if (validateUserResultDto.Outcome == ValidateUserOutcome.Success)
                    {
                        var auditObj = AuditUserActivity
                            .New(validateUserResultDto.UserInfo, AuditUserActivityTypeEnum.SignedIn)
                            .SetDescription("[{0}] Signed In to Domain [{1}]", validateUserResultDto.UserInfo.EffectiveUser.Username, Request.Url.Authority)
                            .SetUserAgent(Request.UserAgent).SetSSOToken(accesstoken);

                        ProviderFactory.Logging.Audit(AuditLevelEnum.Information, AuditTypeEnum.SignedIn,
                                                      string.Format("user signed in with External Access token {0}, Type {1}", accesstoken.ToString(), accessTokenBaseDto.Type()), auditObj,
                                                      validateUserResultDto.UserInfo);
                        FormsAuthentication.SetAuthCookie(validateUserResultDto.UserInfo.ToAuthCookie(), false);
                    }

                    switch (accessTokenBaseDto.Type())
                    {
                        case AccessTokenTypeEnum.PasswordReset:
                            Response.Redirect(Url.RouteUrl("LoginSetPassword", new { reset = "Reset" }), false);
                            break;
                        case AccessTokenTypeEnum.PostApprovalHistory:
                            Response.Redirect(Url.RouteUrl("NotificationApprovalHistory"), false);
                            break;

                        case AccessTokenTypeEnum.PostApproval:
                            PostApprovalAccessTokenDTO approvalAccessTokenDto = accessTokenBaseDto as PostApprovalAccessTokenDTO;
                            Session["MobileApproval_PostTargetID"] = approvalAccessTokenDto.PostTargetID;

                            if (Request.Browser.IsMobileDevice)
                                Response.Redirect(Url.RouteUrl("MobilePostApproval"), false);
                            else
                                Response.Redirect(Url.RouteUrl("NotificationManage"), false);
                            break;

                        case AccessTokenTypeEnum.SyndicationUnsubscribe:
                            SyndicationUnsubscribeDTO syndicationUnsubscribeDto = accessTokenBaseDto as SyndicationUnsubscribeDTO;
                            Session["Syndication_UnSubscribeStatus"] = ProviderFactory.Social.SetSyndicationUnsubscribe(syndicationUnsubscribeDto);

                            Response.Redirect(Url.RouteUrl("SyndicationUnsubscribe"), false);
                            break;

                        case AccessTokenTypeEnum.SocialProductConfig:
                            SocialProductConfigAccessTokenDTO socialProductConfigAccessTokenDto = accessTokenBaseDto as SocialProductConfigAccessTokenDTO;
                            Session["SocialProductConfig_AccountID"] = socialProductConfigAccessTokenDto.AccountID;

                            Response.Redirect(Url.RouteUrl("SocialProductConfigByToken"), false);
                            break;
                    }

                }

                //check other token types
                return Json("", JsonRequestBehavior.AllowGet);

            }

            Request.Abort();
            return Json("", JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region misc

        [HttpPost]
        [Route("/zipcheck")]
        [CompressResponse]
        public JsonResult ZipCheck(ZipCheckRequest req)
        {
            long zip;
            if (!long.TryParse(req.zip, out zip))
                return Json(new BaseResponse() { success = false });

            BaseModel model = new BaseModel();
            ZipCodeDTO dtoZip = ProviderFactory.Lists.GetZipCode(zip);

            if (dtoZip == null)
                return Json(new BaseResponse() { success = false });

            List<CityDTO> dtoCities = ProviderFactory.Lists.GetCitiesForZipCode(dtoZip.ZipCode);

            CheckZipResponse response = new CheckZipResponse() { success = true };
            response.stateid = dtoZip.StateID.ToString();
            if (dtoCities.Count() > 0)
            {
                response.city = dtoCities[0].Name.Trim();
            }
            else
            {
                response.city = null;
            }

            return Json(response);
        }


        #endregion

        #region templates

        [HttpPost]
        [Route("/template")]
        [CompressResponse]
        public JsonResult GetTemplate(TemplateRequest req)
        {
            DialogModel model = new DialogModel();

            switch (req.type.Trim().ToLower())
            {
                case "controller":
                    {
                        string template = this.RenderPartialToString("~/Views/Dialogs/ControllerDialog.cshtml", model);
                        return Json(new TemplateResponse() { success = true, type = req.type, html = template, dialog = model });
                    }
                case "jobtypes":
                    {
                        string template = this.RenderPartialToString("~/Views/Dialogs/JobTypesDialog.cshtml", model);
                        return Json(new TemplateResponse() { success = true, type = req.type, html = template, dialog = model });
                    }
                case "jobtype":
                    {
                        string template = this.RenderPartialToString("~/Views/Dialogs/JobTypeDialog.cshtml", model);
                        return Json(new TemplateResponse() { success = true, type = req.type, html = template, dialog = model });
                    }
            }

            return Json(new TemplateResponse() { success = false, message = "Template not found." });
        }

        #endregion

        #region basic unauthenticated lists

        [HttpPost]
        [Route("/list")]
        [CompressResponse]
        public JsonResult List(ListRequest req)
        {
            switch (req.type.Trim().ToLower())
            {
                //case "packages":
                //    return Json(new ListResponse<PackageDTO>()
                //    {
                //        success = true,
                //        type = req.type,
                //        list = ProviderFactory.Security.GetPackages()
                //    });

                case "franchises":
                    return Json(new ListResponse<FranchiseTypeDTO>()
                    {
                        success = true,
                        type = req.type,
                        list = ProviderFactory.Lists.GetFranchiseTypes()
                    });

                case "crmsystems":
                    return Json(new ListResponse<CRMSystemTypeDTO>()
                    {
                        success = true,
                        type = req.type,
                        list = ProviderFactory.Lists.GetCRMSystemTypes()
                    });

                case "countries":
                    return Json(new ListResponse<CountryDTO>()
                    {
                        success = true,
                        type = req.type,
                        list = ProviderFactory.Lists.GetCountries()
                    }, JsonRequestBehavior.AllowGet);

                case "states":
                    return Json(new ListResponse<StateDTO>()
                    {
                        success = true,
                        type = req.type,
                        list = ProviderFactory.Lists.GetStatesForCountry(1)
                    });

                default:
                    break;
            }

            return Json(new BaseResponse() { success = false });
        }

        [Route("/list")]
        public JsonResult List(string type, int? countryID)
        {
            switch (type.Trim().ToLower())
            {
                //case "packages":
                //    return Json(ProviderFactory.Security.GetPackages(), JsonRequestBehavior.AllowGet);
                case "franchises":
                    return Json(ProviderFactory.Lists.GetFranchiseTypes(), JsonRequestBehavior.AllowGet);
                case "verticalmarkets":
                    return Json(ProviderFactory.Lists.GetVerticalMarkets(), JsonRequestBehavior.AllowGet);
                case "crmsystems":
                    return Json(ProviderFactory.Lists.GetCRMSystemTypes(), JsonRequestBehavior.AllowGet);
                case "countries":
                    return Json(ProviderFactory.Lists.GetCountries(), JsonRequestBehavior.AllowGet);
                case "states":
                    return Json(ProviderFactory.Lists.GetStatesForCountry(countryID.HasValue ? countryID.GetValueOrDefault() : 1), JsonRequestBehavior.AllowGet);
                case "accounttypes":
                    return Json(ProviderFactory.Lists.GetAccountTypes(), JsonRequestBehavior.AllowGet);
                case "timezones":
                    return Json(ProviderFactory.Lists.GetTimeZones(), JsonRequestBehavior.AllowGet);
                case "postcategories":
                    return Json(ProviderFactory.Lists.GetPostCategoriesByVerticalID(1), JsonRequestBehavior.AllowGet);
                default:
                    return null;
            }
        }

        #endregion

        [HttpGet]
        [Authorize]
        [OutputCache(Duration = 36000, VaryByParam = "*", VaryByCustom = "UserIDCache")]
        public JsonResult GetMenu([DataSourceRequest] DataSourceRequest request)
        {
            BaseMembersModel model = new BaseMembersModel();

            UserDTO userDto = model.UserInfo.EffectiveUser;

            List<menu> menuContainer = new List<menu>();

            #region Dashboard
            menu menuDashboard = new menu() { text = "DASHBOARD", url = "" };
            menuDashboard.items = new List<menu>();

            if (userDto.CanDashboardOverviewBasic)
            {
                menuDashboard.items.Add(new menu() { text = "Overview", url = Url.RouteUrl("DashboardOverview") });
            }

            if (userDto.CanSocialDashboard)
            {
                menuDashboard.items.Add(new menu() { text = "Social", url = Url.RouteUrl("DashboardSocial") });
            }

            if (userDto.CanDashboardReputationLocation || userDto.CanSocialLocation)
            {
                menu menuLocations = new menu() { text = "Locations", url = Url.RouteUrl("DashboardReputationLocations") };
                menuLocations.items = new List<menu>();

                if (userDto.CanDashboardReputationLocation)
                    menuLocations.items.Add(new menu() { text = "Reputation", url = Url.RouteUrl("DashboardReputationLocations") });

                if (userDto.CanSocialLocation)
                    menuLocations.items.Add(new menu() { text = "Social", url = Url.RouteUrl("DashboardSocialLocations") });

                if (menuLocations.items.Any())
                    menuDashboard.items.Add(menuLocations);
            }

            if (menuDashboard.items.Any())
                menuContainer.Add(menuDashboard);
            #endregion

            #region Reputation
            menu menuReputation = new menu() { text = "REPUTATION", url = "" };
            menuReputation.items = new List<menu>();

            if (userDto.CanReputationStream)
                menuReputation.items.Add(new menu() { text = "Review Stream", url = Url.RouteUrl("ReputationReviewStream") });
            if (userDto.CanReputationSites)
                menuReputation.items.Add(new menu() { text = "Review Sites", url = Url.RouteUrl("ReputationReviewSites") });
            if (userDto.CanCompetitiveAnalysis)
                menuReputation.items.Add(new menu() { text = "Competitive Analysis", url = Url.RouteUrl("ReputationCompetitiveAnalysisReport") });

            if (menuReputation.items.Any())
                menuContainer.Add(menuReputation);
            #endregion

            #region Social
            menu menuSocial = new menu() { text = "SOCIAL", url = "" };
            menuSocial.items = new List<menu>();

            if (userDto.CanPublish)
                menuSocial.items.Add(new menu() { text = "Publish", url = Url.RouteUrl("SocialPublish") });
            if (userDto.CanDealerPostActivity)
                menuSocial.items.Add(new menu() { text = "Post Activity", url = Url.RouteUrl("SocialDealerPostActivity") });
            if (userDto.CanSocialPostManagement)
                menuSocial.items.Add(new menu() { text = "Post Management", url = Url.RouteUrl("SocialPostManagement") });
            if (userDto.CanPublish)
                menuSocial.items.Add(new menu() { text = "Content Library", url = Url.RouteUrl("SocialContentLibrary") });
            if (userDto.CanDealerPostActivity)
                menuSocial.items.Add(new menu() { text = "Post Calendar", url = Url.RouteUrl("SocialPostCalendar") });
            if (userDto.CanSocialStream)
                menuSocial.items.Add(new menu() { text = "Profile Manager", url = Url.RouteUrl("SocialStream") });
            if (userDto.CanPublish)
                menuSocial.items.Add(new menu() { text = "Post Statistics", url = Url.RouteUrl("SocialPostStatistics") });

            if (userDto.CanApprovalHistory)
                menuSocial.items.Add(new menu() { text = "Approval History", url = Url.RouteUrl("NotificationApprovalHistory") });

            if (menuSocial.items.Any())
                menuContainer.Add(menuSocial);
            #endregion

            #region Reports
            if (userDto.CanReportsMenu)
            {
                menu menuReports = new menu() { text = "REPORTS", url = "" };
                menuReports.items = new List<menu>();

                //if (userDto.CanReportSAM)
                //	menuReports.items.Add(new menu() { text = "SAM Report", url = Url.RouteUrl("ReportsSAMReport") });
                if (userDto.CanMenuReportsPostPerformance)
                    menuReports.items.Add(new menu() { text = "Post Performance", url = Url.RouteUrl("TelerikReportViewer_PostPerformance") });

                if (userDto.CanMenuSocialSnapshot)
                    menuReports.items.Add(new menu() { text = "Social Snapshot", url = Url.RouteUrl("SocialSnapshotReport") });

                if (userDto.CanMenuReputationSnapshot)
                    menuReports.items.Add(new menu() { text = "Reputation Snapshot", url = Url.RouteUrl("ReputationSnapshotReport") });

                if (userDto.HasMultipleAccountsWithPaidProducts)
                    menuReports.items.Add(new menu() { text = "Enterprise Reputation & Social Report", url = Url.RouteUrl("EnterpriseReputationSocialReport2") });

                if (userDto.CanViewReputationReports)
                {
                    //    menuReports.items.Add(new menu() { text = "Reputation Monthly Ratings", url = Url.RouteUrl("TelerikReportViewer_CompleteDealerReputationReport-MonthlyRatings") });
                    //    menuReports.items.Add(new menu() { text = "Reputation Monthly Review Totals", url = Url.RouteUrl("TelerikReportViewer_CompleteDealerReputationReport-MonthlyReviewTotals") });
                    menuReports.items.Add(new menu() { text = "Reputation Summary", url = Url.RouteUrl("TelerikReportViewer_CompleteDealerReputationReport-Summary") });
                }

                if (userDto.CanMenuReportPostSummary)
                {
                    menuReports.items.Add(new menu() { text = "Post Summary Report", url = Url.RouteUrl("TelerikReportViewer_PostSummary") });
                }

                //if (userDto.CanViewSocialReports)
                //{
                //    menuReports.items.Add(new menu() { text = "Enterprise Social Network", url = Url.RouteUrl("TelerikReportViewer_EnterpriseSocialNetworkReport") });
                //    menuReports.items.Add(new menu() { text = "Social Media", url = Url.RouteUrl("TelerikReportViewer_SocialMediaReport") });
                //}

                if (menuReports.items.Any())
                    menuContainer.Add(menuReports);
            }
            #endregion

            #region Admin
            menu menuAdmin = new menu() { text = "ADMIN", url = "" };
            menuAdmin.items = new List<menu>();
            if (userDto.CanAdminAccount)
                menuAdmin.items.Add(new menu() { text = "Accounts", url = Url.RouteUrl("AdminAccounts") });
            if (userDto.CanAdminUsers)
                menuAdmin.items.Add(new menu() { text = "Users", url = Url.RouteUrl("AdminUsers") });
            if (userDto.CanAdminVirtualGroups)
                menuAdmin.items.Add(new menu() { text = "Virtual Groups", url = Url.RouteUrl("AdminVirtualGroups") });


            if (userDto.CanAdminSystem)
                menuAdmin.items.Add(new menu() { text = "System", url = Url.RouteUrl("MembersSystem") });
            if (userDto.CanAdminExceptions)
                menuAdmin.items.Add(new menu() { text = "Exceptions", url = Url.RouteUrl("ExceptionLog") });
            if (userDto.CanAdminFlushCache)
                menuAdmin.items.Add(new menu() { text = "Flush Server Caches", url = Url.RouteUrl("FlushServerCaches") });

            if (menuAdmin.items.Any())
                menuContainer.Add(menuAdmin);
            #endregion

            #region SupportCenter
            menu menuSC = new menu() { text = "SUPPORT CENTER", url = "" };
            menuSC.items = new List<menu>();

            if (userDto.CanSupportCenterThemes)
                menuSC.items.Add(new menu() { text = "Themes & Vertical", url = "javascript:void(0);SIGlobal.showSupportCenterWindow(\"ThemesVertical\");" });
            if (userDto.CanSupportCenterPackages)
                menuSC.items.Add(new menu() { text = "Packages", url = "javascript:void(0);SIGlobal.showSupportCenterWindow(\"Packages\");" });
            if (userDto.CanSupportCenterEmployees)
                menuSC.items.Add(new menu() { text = "Employees", url = "javascript:void(0);SIGlobal.showSupportCenterWindow(\"Employees\");" });
            if (userDto.CanSupportCenterPlatformMessages)
                menuSC.items.Add(new menu() { text = "Platform Messages", url = "javascript:void(0);SIGlobal.showSupportCenterWindow(\"PlatformMessages\");" });
            if (userDto.CanSupportCenterSingleSignOn)
                menuSC.items.Add(new menu() { text = "Single-Sign-On", url = "javascript:void(0);SIGlobal.showSupportCenterWindow(\"SingleSignOn\");" });
            if (userDto.CanSupportCenterAccountManagers)
                menuSC.items.Add(new menu() { text = "Account Managers", url = "javascript:void(0);SIGlobal.showSupportCenterWindow(\"AccountManagers\");" });

            if (menuSC.items.Any())
                menuContainer.Add(menuSC);

            #endregion

            return Json(menuContainer.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public class menu
        {
            public string text { get; set; }
            public string url { get; set; }
            public string imageUrl { get; set; }
            public List<menu> items { get; set; }
        }


        [Route("oAuth/Callback", RouteName = "OAuthCallback")]
        public ActionResult OAuthCallback(string code, string state)
        {
            AddAuthCodeToCache(code, state);

            return View();
        }

        public JsonResult GetSocialAuthCode(string stateContext)
        {
            ObjectCache cache = MemoryCache.Default;

            Dictionary<string, string> socialAuthCache = cache["socialAuthCache"] as Dictionary<string, string>;
            if (socialAuthCache != null)
            {
                if (socialAuthCache.ContainsKey(stateContext))
                {
                    return Json(new { code = socialAuthCache[stateContext] }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { code = "" }, JsonRequestBehavior.AllowGet);
        }



        [Route("oAuth/TwitterCallback", RouteName = "TwitterCallback")]
        public ActionResult TwitterCallback(string oauth_token, string oauth_verifier, string context)
        {
            AddAuthCodeToCache(string.Format("{0},{1}", oauth_token, oauth_verifier), context);

            return View("OAuthCallback");
        }

        private void AddAuthCodeToCache(string code, string state)
        {
            ObjectCache cache = MemoryCache.Default;

            Dictionary<string, string> socialAuthCache = cache["socialAuthCache"] as Dictionary<string, string>;
            if (socialAuthCache == null)
            {
                CacheItemPolicy policy = new CacheItemPolicy();
                policy.SlidingExpiration = new TimeSpan(0, 1, 0);

                socialAuthCache = new Dictionary<string, string>();
                socialAuthCache.Add(state, code);

                cache.Set("socialAuthCache", socialAuthCache, policy);
            }
            else
            {
                if (socialAuthCache.ContainsKey(state))
                    socialAuthCache[state] = code;
                else
                    socialAuthCache.Add(state, code);

                cache["socialAuthCache"] = socialAuthCache;
            }

        }

    }
}
