﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AttributeRouting.Web.Mvc;
using SI.BLL;
using SI.DTO;
using SI.Services.Admin;
using SI.Services.Dashboard;
using SI.Services.Dashboard.Models;
using SI.Services.Public;
using SI.WEB.Portal.Framework;
using SI.WEB.Portal.Models;

namespace SI.WEB.Portal.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            BaseModel model = new BaseModel();
            ViewBag.BaseModel = model;
            ViewBag.ReturnUrl = returnUrl;

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
				if (AdminService.ValidateUser(model.UserName, model.Password, model.RememberMe, Request.Url.Authority, Request.UserAgent))
                {
	                return RedirectToLocal(returnUrl);
                }
            }

            ModelState.AddModelError("", "The user name or password provided is incorrect.");  //Locale.Localize("usernotfound") WAS NOT FOUND
            BaseModel basemodel = new BaseModel();
            ViewBag.BaseModel = basemodel;

            return View(model);
        }

        [Route("/Account/Impersonate/{userID}", RouteName = "Impersonate")]
        public JsonResult Impersonate(long userID)
        {
            BaseMembersModel model = new BaseMembersModel();
            if (model.UserInfo != null)
            {
                return Json(AdminService.ImpersonateUser(model.UserInfo, userID), JsonRequestBehavior.AllowGet);
            }
            else
            {
				return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("/Account/StopImpersonation", RouteName = "StopImpersonation")]
        public void StopImpersonation()
        {
            BaseMembersModel model = new BaseMembersModel();
            if (model.UserInfo != null) {
                AdminService.StopImpersonation(model.UserInfo);
            }
        }

        [Route("/Account/LogOff", RouteName = "LogOff")]
        public ActionResult LogOff()
        {
            BaseMembersModel model = new BaseMembersModel();            
            FormsAuthentication.SignOut();
            if (model.UserInfo != null)
            {
                AdminService.UserSignedOut(model.UserInfo, Request.Url.Authority);
            }
            Session.Abandon();

            return RedirectToAction("Login", "Account");
        }
		
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
		public async Task<ActionResult> SendResetPasswordEmail(string userName)
        {
	        bool success = false;
			if (!string.IsNullOrWhiteSpace(userName))
			{
				string callback = string.Format("{0}://{1}/access", Request.Url.Scheme, Request.Url.Authority);
				success = await PublicService.SendRestPasswordEmail(userName, callback);
			}

	        return Json(new {success});
        }

		private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
                return Redirect(returnUrl);
            
            return RedirectToAction("Index", "Public");
        }

		[HttpGet]
	    [Route("/Account/SetPassword/{*reset}", RouteName = "LoginSetPassword")]
		public ActionResult SetPassword(string reset = "")
	    {
			BaseMembersModel model = new BaseMembersModel();
			ViewBag.BaseModel = model;

			ViewBag.IsResetPassword = false;
			if (!string.IsNullOrEmpty(reset))
			{
				if (reset == "Reset")
					ViewBag.IsResetPassword = true;
			}
			else if (model.UserInfo.EffectiveUser.Password != Security.GetPasswordHash("Password$"))
			{
				return RedirectToAction("Index", "Public");
			}

			SetPassword setPassword = new SetPassword();

			return View("SetPassword", setPassword);
	    }

	    [HttpPost]
		[ValidateAntiForgeryToken]
		public JsonResult SetPassword(SetPassword setPassword)
		{
			BaseMembersModel bmm = new BaseMembersModel();

			string modelErrors = "";
			bool success = false;

			if (ModelState.IsValid)
			{
				string Problem = "";
				MyPrefPassword myPrefPassword = new MyPrefPassword();
				myPrefPassword.CurrentPassword = null;
				myPrefPassword.Password = setPassword.Password;
				myPrefPassword.VerifyPassword = setPassword.VerifyPassword;

				success = DashboardService.UserPreferencesPassword(bmm.UserInfo, myPrefPassword, ref Problem);

				if (!success)
					modelErrors = Problem;
			}
			else
				modelErrors = SIHelpers.ModelErrorsToHtml(ModelState);

			string returnStatus;

			if (success)
				returnStatus = "Password Saved, please login. Your User Name is " + bmm.UserInfo.EffectiveUser.Username;
			else
				returnStatus = string.Format("Unable to save at this time. {0}", modelErrors);

			return Json(new { success, message = returnStatus });
		}

    }
}
