﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;

namespace SI.WEB.Portal.Controllers
{
    public class AssetsController : SuperController
    {

		[Route("style/{themeName}/{cssHash}", ActionPrecedence = 200)]
        [CompressResponse]
        [CacheFilter(Duration = 600000)]
        public FileResult Style(string themeName, string cssHash)
        {
            return File(MvcApplication.AssetManager.GetThemeCSS(themeName), "text/css");
        }

		[Route("style/{themeName}/sprite/{spriteHash}", ActionPrecedence = 100)]
        [CacheFilter(Duration = 600000)]
        public FileResult Sprite(string themeName, string spriteHash)
        {
            return File(MvcApplication.AssetManager.GetThemeSpriteImage(themeName), "image/png");
        }

		[Route("script/{scriptHash}", ActionPrecedence = 200)]
        [CompressResponse]
        [CacheFilter(Duration = 600000)]
        public FileResult Script(string scriptHash)
        {
            return File(MvcApplication.AssetManager.JSData, "text/javascript");
        }
        
    }
}
