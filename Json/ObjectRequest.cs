﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SI.WEB.Portal.Json
{
    public class ObjectRequest
    {
        public long id { get; set; }
        public long? pid { get; set; }
    }
}