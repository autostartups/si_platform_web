﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SI.WEB.Portal.Json
{
    public class TemplateRequest
    {
        /// <summary>
        /// The type of template to retrieve
        /// </summary>
        public string type { get; set; }
    }
}