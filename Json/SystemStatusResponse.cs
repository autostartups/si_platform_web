﻿using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SI.WEB.Portal.Json
{
    public class SystemStatusResponse : BaseResponse
    {
        public SystemStatusDTO Status { get; set; }
    }
}