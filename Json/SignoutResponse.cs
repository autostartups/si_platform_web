﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SI.WEB.Portal.Json
{
    public class SignoutResponse : BaseResponse
    {
        public string url { get; set; }
    }
}