﻿using SI.DTO;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;

namespace SI.WEB.Portal.Json
{
    public class AccountModel : BaseResponse
    {
        public AccountDTO Account { get; set; }
        public AccountOptionsDTO Opts { get; set; }
		//public long[] FranchiseTypeIDs { get; set; }
    }
}