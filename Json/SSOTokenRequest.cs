﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SI.WEB.Portal.Json
{
    public class SSOTokenRequest
    {
        public Guid ResellerKey { get; set; }
        public Guid UserKey { get; set; }
    }
}