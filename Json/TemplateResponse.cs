﻿using SI.WEB.Portal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SI.WEB.Portal.Json
{
    public class TemplateResponse : BaseResponse
    {
        public string type { get; set; }
        public string html { get; set; }
        public DialogModel dialog { get; set; }
    }
}