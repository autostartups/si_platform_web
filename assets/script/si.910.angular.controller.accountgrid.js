﻿
app.controller('SIAccountSearchController', function ($scope, $http, $timeout) {

    $scope.franchises = SILists.get('franchises');
    $scope.packages = SILists.get('packages');
    $scope.perpages = [{ ID: 15, Name: '15 Per Page' }, { ID: 25, Name: '25 Per Page' }, { ID: 50, Name: '50 Per Page' }];

    $scope.showNext = true;
    $scope.showPrev = false;

    $scope.filters = {};
    $scope.filters.FranchiseTypeIDs = [];
    $scope.filters.PackageIDs = [];
    $scope.filters.SearchString = '';
    $scope.filters.HasChildAccounts = false;
    $scope.filters.SortBy = 1;
    $scope.filters.SortAscending = true;
    $scope.filters.StartIndex = 1;
    $scope.filters.EndIndex = 15;
    $scope.filters.PerPage = 15;
    
    $scope.results = {};
    $scope.results.TotalRecords = 0;
    $scope.results.StartIndex = 1;
    $scope.results.EndIndex = 15;
    $scope.results.Items = [];

    $scope.promchange = null;
    $scope.loading = false;

    $scope.reload = function () {
        $scope.loading = true;
        $scope.filters.EndIndex = $scope.filters.StartIndex + ($scope.filters.PerPage - 1);
        $('#si-wait').show();
        $http({
            url: '/members/getaccounts',
            data: $.j2MVC($scope.filters),
            method: 'POST'
        }).success(function (data, status, headers, config) {
            $scope.results = data;
            $scope.loading = false;
            $scope.showNext = $scope.results.EndIndex < $scope.results.TotalRecords;
            $scope.showPrev = $scope.results.StartIndex > 1;
            $('#si-wait').hide();
        }).error(function (data, status, headers, config) {
            $scope.loading = false;
            $('#si-wait').hide();
        });
    }

    $scope.reload();

    $scope.$watch('filters.SearchString', function (val) {
        $scope.filters.StartIndex = 1;
        $scope.change();
    });
    $scope.$watch('filters.FranchiseTypeIDs', function (val) {
        $scope.filters.StartIndex = 1;
        $scope.change();
    });
    $scope.$watch('filters.PackageIDs', function (val) {
        $scope.filters.StartIndex = 1;
        $scope.change();
    });
    $scope.$watch('filters.AccountTypeIDs', function (val) {
        $scope.filters.StartIndex = 1;
        $scope.change();
    });
    $scope.$watch('filters.PerPage', function (val) {
        $scope.filters.StartIndex = 1;
        $scope.change();
    });

    $scope.change = function () {
        if ($scope.loading) return;
                
        if ($scope.promchange != null) {
            $timeout.cancel($scope.promchange);
        }
        $scope.promchange = $timeout($scope.reload, 250, true);
    }

    $scope.rfid = function (item) {
        var index = $scope.filters.FranchiseTypeIDs.indexOf(item)

        var t = [];
        angular.copy($scope.filters.FranchiseTypeIDs, t);
        t.splice(index, 1);
        $scope.filters.FranchiseTypeIDs = t;
        
    }
    $scope.rpck = function (item) {
        var index = $scope.filters.PackageIDs.indexOf(item)
        
        var t = [];
        angular.copy($scope.filters.PackageIDs, t);
        t.splice(index, 1);
        $scope.filters.PackageIDs = t;
    }

    $scope.getById = function(items, id) {
        for (var i = 0; i < items.length; i++) {
            if (items[i].ID==id)
                return items[i].Name;
        }
        return '';
    }

    $scope.endIndex = function () {
        if ($scope.filters.StartIndex + $scope.filters.PerPage >= $scope.filters.TotalRecords)
            return $scope.TotalRecords;
        return $scope.filters.StartIndex + ($scope.filters.PerPage-1);
    }


    $scope.nextPage = function () {
        $scope.filters.StartIndex += $scope.filters.PerPage;
        $scope.reload();
    }

    $scope.prevPage = function () {
        if ($scope.filters.StartIndex < $scope.filters.PerPage) return;
        $scope.filters.StartIndex -= $scope.filters.PerPage;
        $scope.reload();
    }
});