﻿/* AngularJS Controller and related routines for the account dialog */

app.controller('SIAccountController', function ($scope) {

    if ($scope.loaded == true) return;
    $scope.loaded = true;

    // load the lists we need for dropdowns and such

    $scope.franchises = SILists.get('franchises');
    $scope.crmsystems = SILists.get('crmsystems');
    $scope.states = SILists.get('states');
    $scope.countries = SILists.get('countries');

    // display control fields, miscellaneous fields to use for display purposes

    $scope.title = 'New Account';

    $scope.error = null;

    // watchers

    $scope.$watch('title', function (val) {
        $('#' + $scope.dlgkey).dialog("option", "title", val);
    });
    $scope.$watch('account.DisplayName', function (val) {
        $scope.title_update();
    });
    $scope.$watch('account.Name', function (val) {
        $scope.title_update();
    });


    // misc functions

    $scope.title_update = function () {
        if ($scope.account != null) {
            var t = $scope.account.DisplayName;
            if (t == null || t == '') t = $scope.account.Name;
            if (t == null || t == '') t = 'New Account';
            if ($scope.account.ID == 0)
                t = t + '*';
            $scope.title = t;
        }
    }

    // load a model into the scope

    $scope.load = function (model) {
        if (model.success) {
            $scope.account = model.Account;            
            $scope.Opts = model.Opts;

            $scope.title_update();

        } else {
            $scope.error = model.message;
        }
    }

    // save data

    $scope.save = function () {
        var model = ajxs('/members/' + $scope.type, { id: $scope.id, pid: $scope.pid }, 'PUT');
        $scope.load(model);
    }

    // load the raw model
    $scope.load($scope.rawModel);

});