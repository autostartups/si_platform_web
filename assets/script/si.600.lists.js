﻿/**********************************************************

SI List Manager

This object lazy loads the various reusable lists used
by drop downs and lookups.

**********************************************************/

var SILists = new SIListManager();
function SIListManager() {

    this.get = function (type) {
        if (this.cache.hasOwnProperty(type)) {
            if (this.cache[type].length == 0) {
                var ret = ajxs('/list', { type: type });
                if (ret.success) {
                    for (var i = 0; i < ret.list.length; i++) {
                        this.cache[type].push(ret.list[i]);
                    }
                }
            }
            return this.cache[type];
        }
        var ret = ajxs('/list', { type: type });
        if (ret.success) {
            this.cache[ret.type] = ret.list;
            return this.cache[type];
        }
        return null;
    }

    this.reload = function (type) {
        this.cache[type].length = 0;
        this.get(type);
    }

    this.clear = function () {
        this.cache = {};
    }
    
    this.clear();
}