/*
*       OPTIONS LISTING:
*           *listboxFromView, listboxToView         - the id attributes of the VISIBLE listboxes
*           *moveToFrom, moveToTo                   - the id attributes of the elements used to transfer only selected items between boxes
*           *allmoveToFrom, allmoveToTo             - the id attributes of the elements used to transfer ALL (visible) items between boxes
*           *sortBy                     - the attribute to use when sorting items (values: 'text' or 'value')
*           *useSorting                 - sort items after executing a transfer (true/false)
*           *selectOnSubmit             - select items in box 2 when the form is submitted (true/false)
*
*/

(function ($) {
	var settings = [];
	var group1 = [];
	var group2 = [];
	var onSort = [];

	//the main method that the end user will execute to setup the DLB
	$.dualListBoxes = function (options) {
		//define default settings
		var index = settings.push({
			listboxFromView: 'listBoxFrom',
			listboxToView: 'listBoxTo',
			moveToFrom: 'moveToFrom',
			allmoveToFrom: 'moveAllToFrom',
			moveToTo: 'moveToTo',
			allmoveToTo: 'moveAllToTo',
			sortBy: 'text',
			useSorting: true,
			selectOnSubmit: false
		});

		index--;

		//merge default settings w/ user defined settings (with user-defined settings overriding defaults)
		$.extend(settings[index], options);

		//define box groups
		group1.push({
			view: settings[index].listboxFromView,
			index: index
		});
		group2.push({
			view: settings[index].listboxToView,
			index: index
		});

		//define sort function
		if (settings[index].sortBy == 'text') {
			onSort.push(function (a, b) {
				var aVal = a.text.toLowerCase();
				var bVal = b.text.toLowerCase();
				if (aVal < bVal) { return -1; }
				if (aVal > bVal) { return 1; }
				return 0;
			});
		} else {
			onSort.push(function (a, b) {
				var aVal = a.value.toLowerCase();
				var bVal = b.value.toLowerCase();
				if (aVal < bVal) { return -1; }
				if (aVal > bVal) { return 1; }
				return 0;
			});
		}

		//configure events
		$('#' + group2[index].view).dblclick(function () {
			MoveSelected(group2[index], group1[index]);
		});
		$('#' + group2[index].view).keypress(function (e) {
			if (e.which == 13)
				MoveSelected(group2[index], group1[index]);
		});

		$('#' + settings[index].moveToFrom).click(function () {
			MoveSelected(group2[index], group1[index]);
		});
		$('#' + settings[index].allmoveToFrom).click(function () {
			MoveAll(group2[index], group1[index]);
		});
	
		$('#' + group1[index].view).dblclick(function () {
			MoveSelected(group1[index], group2[index]);
		});
		$('#' + group1[index].view).keypress(function (e) {
			if (e.which == 13)
				MoveSelected(group1[index], group2[index]);
		});

		$('#' + settings[index].moveToTo).click(function () {
			MoveSelected(group1[index], group2[index]);
		});
		$('#' + settings[index].allmoveToTo).click(function () {
			MoveAll(group1[index], group2[index]);
		});

		//pre-sort item sets
		if (settings[index].useSorting) {
			SortOptions(group1[index]);
			SortOptions(group2[index]);
		}

		//attach onSubmit functionality if desired
		if (settings[index].selectOnSubmit) {
			$('#' + settings[index].listboxToView).closest('form').submit(function () {
				$('#' + settings[index].listboxToView).children('option').attr('selected', 'selected');
			});
		}

		
	};
	


	function SortOptions(group) {
		var $toSortOptions = $('#' + group.view + ' option');
		$toSortOptions.sort(onSort[group.index]);
		$('#' + group.view).empty().append($toSortOptions);
	}

	function MoveSelected(fromGroup, toGroup) {
		$('#' + fromGroup.view + ' option:selected,#' + fromGroup.view + ' option[selected=selected]').appendTo('#' + toGroup.view);
		$('#' + fromGroup.view + ' option,#' + toGroup.view + ' option').removeAttr('selected');
		SortOptions(toGroup);
	}

	function MoveAll(fromGroup, toGroup) {
		$('#' + fromGroup.view + ' option').appendTo('#' + toGroup.view);
		$('#' + fromGroup.view + ' option,#' + toGroup.view + ' option').removeAttr('selected');
		SortOptions(toGroup);
	}

})(jQuery);