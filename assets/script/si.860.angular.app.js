﻿var app = angular.module('angularjs-si', ['$strap.directives', 'kendo.directives']);

app.controller('SIMainController', function ($scope, $http, $compile) {

    var ascope = appScope();
    ascope.dlg = null;

    /* dialog generator */

    $scope.dialog = function (type, id, pid) {
        var key = si_dlg_key(type, id, pid);

        if ($('#' + key).length > 0) {
            $('#' + key).dialog('moveToTop');
            return;
        }

        //load the template
        var tpl = SITemplates.get(type);

        //set the dialog peroperties so the underlying controller can load it's data
        var dscope = $scope.$new(true);
        dscope.id = id;
        dscope.pid = pid;
        dscope.type = type;
        dscope.dlgkey = key;

        //load the model
        var model = ajxs('/members/' + dscope.type, { id: dscope.id, pid: dscope.pid }, 'GET');
        if (!model.success) {
        	$.when(kendo.ui.ExtAlertDialog.show({
        		title: model.title,
        		message: model.message,
        		icon: "k-ext-error"
        	}));
            return;
        }
        dscope.rawModel = model;

        //create the dom elements
        var elm = $('<div>');
        elm.html(tpl.html);
        elm = elm.firstChild().addClass('hideme').attr('id', key);
        $('body').append(elm);

        //compile the template
        $compile(elm)(dscope);

        elm
            .removeClass('hideme')
            .dialog({
                modal: tpl.dialog.Modal,
                height: 'auto', minWidth: tpl.dialog.Width,
                resizable: tpl.dialog.Resizable,
                position: 'center',
                close: function (event, ui) {
                    var scope = appScope();
                    scope.dlg.count--;

                    angular.element($(this)).scope().$destroy();
                    $(this).remove();
                }
            });

        
        //var h = $(elm).height();
        //var w = $(elm).width();
        //var vh = jQuery(window).height();
        //var vw = jQuery(window).width();

        //var mpos = $('#l-menu').offset();

        //if (ascope.dlg == null || ascope.dlg.count == 0) {
        //    ascope.dlg = {};
        //    ascope.dlg.y = mpos.top + 40;
        //    ascope.dlg.x = mpos.left + 30;
        //    ascope.dlg.count = 0;
        //} else {
        //    ascope.dlg.y += 20;
        //    ascope.dlg.x += 20;
        //}

        //ascope.dlg.count++;

        //if ((ascope.dlg.x + w > vw) || (ascope.dlg.y + h > vh)) {
        //    ascope.dlg.y = mpos.top + 10;
        //    ascope.dlg.x = mpos.left + 10;
        //}

        //$(elm).dialog({ 'position': [ascope.dlg.x, ascope.dlg.y] });


        siInit();

    }


	$scope.setContext = function(id) {
		//console.log(id);

		$http({
			url: '/members/setaccount',
			data: {id: id},
			method: 'POST'
		}).success(function (data, status, headers, config) {
			window.location.reload();
		});
	}

});

function si_dlg(type, id, parent) {
    appScope().$apply(function ($scope) {
        $scope.dialog(type, id, parent);
    });
}
function si_dlg_key(type, id, pid) {
    var key = type + '-' + id + '-' + pid;
    return key;
}
