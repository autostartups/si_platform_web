﻿$.j2MVC = function (value) {
    var result = {};

    var bRes = function (object, prefix) {
        for (var key in object) {

            var postKey = isFinite(key)
				? (prefix != "" ? prefix : "") + "[" + key + "]"
				: (prefix != "" ? prefix + "." : "") + key;
            if (object[key] == null) {
                result[postKey] = 'null';
                break;
            } else {
                switch (typeof (object[key])) {
                    case "number": case "string": case "boolean":
                        result[postKey] = object[key];
                        break;

                    case "object":
                        if (object[key].toUTCString)
                            result[postKey] = object[key].toUTCString().replace("UTC", "GMT");
                        else {
                            bRes(object[key], postKey != "" ? postKey : key);
                        }
                }
            }
        }
    };

    bRes(value, "");

    return result;
};

$.fn.firstChild = function () {
    var ret = [];
    // use a for loop
    for (var i = 0, len = this.length; i < len; i++) {
        var this_el = this[i],
            el = this_el.firstElementChild; // try firstElementChild first
        if (!el) {
            el = this_el.firstChild;
            while (el && el.nodeType != 1)
                el = el.nextSibling;
        }
        if (el) ret.push(el);
    }
    //maintain jQuery chaining and end() functionality
    return this.pushStack(ret);
};

$.fn.posOver = function (elm) {
    return this.each(function () {
        var target = $(this);
        var pos = elm.position();

        var x = pos.left - (target.outerWidth() - elm.outerWidth());
        var y = pos.top;

        target.css({
            position: 'absolute',
            zIndex: 9000,
            top: y,
            left: x
        });
    });
};


$.fn.moveTo = function (selector) {
    return this.each(function () {
        var elm = $(this);
        elm.detach();
        $(elm).appendTo($(selector));
    });
};


$.fn.watch = function (props, callback, timeout) {
    if (!timeout)
        timeout = 10;
    return this.each(function () {
        var el = $(this),
			func = function () { __check.call(this, el) },
			data = {
			    props: props.split(","),
			    func: callback,
			    vals: []
			};
        $.each(data.props, function (i) { data.vals[i] = el.css(data.props[i]); });
        el.data(data);
        if (typeof (this.onpropertychange) == "object") {
            el.bind("propertychange", callback);
        } else if ($.browser.mozilla) {
            el.bind("DOMAttrModified", callback);
        } else {
            setInterval(func, timeout);
        }
    });
    function __check(el) {
        var data = el.data(),
			changed = false,
			temp = "";
        for (var i = 0; i < data.props.length; i++) {
            temp = el.css(data.props[i]);
            if (data.vals[i] != temp) {
                data.vals[i] = temp;
                changed = true;
                break;
            }
        }
        if (changed && data.func) {
            data.func.call(el, data);
        }
    }
}

function ajx(url, data, done, type) {
    if (type == null || type == undefined)
        type = 'POST';

    $.ajax({
        async: true,
        type: type,
        url: url,
        data: $.j2MVC(data),
        success: done
    });
}
function ajxs(url, data, type) {
    $('#si-wait').show();

    if (type == null || type == undefined)
        type = 'POST';

    var ret = $.ajax({
        async: false,
        type: type,
        url: url,
        data: $.j2MVC(data)
    });

    $('#si-wait').hide();

    if (ret.success) {
        var obj = eval('(' + ret.responseText + ')');
        return obj;
    }
    return null;
}

function signIn() {


    ajx(
        '/signin',
        {
            "username": $('#siuser').val(),
            "password": $('#sipwd').val(),
            "persist": false
        }, function (rval) {
            if (rval.success == true) {
                var win = window;
                var loc = location;
                win.location = rval.url;
            }
            else {
                yesno(rval.title, rval.message, null, null, true);
            };
        });
}

function signOut() {
    ajx("/signout", null, function (rval) { var win = window; win.location = rval.url; });
    return false;
}

function appScope() {
    return angular.element($('#si-app')).scope();
}

