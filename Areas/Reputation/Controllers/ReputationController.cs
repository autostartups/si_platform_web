﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
//using SI.DTO.Models.Reputation.ReviewDialogs;
using SI.Extensions;
using SI.Services.Dashboard.Models;
using SI.Services.Reputation;
using SI.Services.Reputation.Models;
using SI.Services.Social;
using SI.WEB.Portal.Framework;
using SI.WEB.Portal.Models;

namespace SI.WEB.Portal.Areas.Reputation.Controllers
{
	[Authorize]
	[RouteArea("Reputation")]
    public class ReputationController : Controller
    {

		[Route("ReviewStream/{*filterParameters}", RouteName = "ReputationReviewStream")]
		[BaseMembersModelAction("CanReputationStream")]
		public ActionResult ReviewStream(string filterParameters = "")
        {
			BaseMembersModel model = ViewBag.baseModel as BaseMembersModel;

			ViewBag.filterParameters = filterParameters;
			ViewBag.LocationName = "";

			if (string.IsNullOrEmpty(filterParameters))
				ViewBag.filterParameters = filterParameters = string.Format("locations,eq,{0}", model.UserInfo.EffectiveUser.CurrentContextAccountID);

			if (filterParameters.IndexOf("locations") >= 0)
			{
				string[] filters = filterParameters.Split('-');
				foreach (string filter in filters)
				{
					string[] fvalues = filter.Split(',');
					if (fvalues[0] == "locations")
					{
						ViewBag.LocationName = SocialService.GetAccountName(fvalues[2].ToLong());
						break;
					}
				}
			}

			return View();
        }

		[HttpPost]
		[Route("ReviewStream", RouteName = "ReputationReviewStreamFilter")]
		[BaseMembersModelAction("CanReputationStream")]
		public ActionResult ReviewStreamFilter(string filterParameters)
		{
			BaseMembersModel model = ViewBag.baseModel as BaseMembersModel;

			filterParameters = filterParameters.IsNullOptional("");

			ViewBag.filterParameters = filterParameters;
			ViewBag.LocationName = "";

			if (string.IsNullOrEmpty(filterParameters))
				ViewBag.filterParameters = filterParameters = string.Format("locations,eq,{0}", model.UserInfo.EffectiveUser.CurrentContextAccountID);

			if (filterParameters.IndexOf("locations") >= 0)
			{
				string[] filters = filterParameters.Split('-');
				foreach (string filter in filters)
				{
					string[] fvalues = filter.Split(',');
					if (fvalues[0] == "locations")
					{
						ViewBag.LocationName = SocialService.GetAccountName(fvalues[2].ToLong());
						break;
					}
				}
			}

			return View("ReviewStream");
		}


		[Route("ReviewSites", RouteName = "ReputationReviewSites")]
		[BaseMembersModelAction("CanReputationSites")]
		public ActionResult ReviewSites()
		{
			return View();
		}

		[HttpPost]
		[Route("ReviewSites", RouteName = "ReputationReviewSitesFilter")]
		[BaseMembersModelAction("CanReputationSites")]
		public ActionResult ReviewSitesFilter(string filterParameters)
		{
			ViewBag.filterParameters = filterParameters;
			ViewBag.LocationName = "";

			return View("ReviewSites");
		}

		[HttpPost]
		public ActionResult ReviewSourceLast6Month()
		{
			BaseMembersModel model = new BaseMembersModel();

			return Json(ReputationService.ReviewSitesReviews(model.UserInfo));
		}


		[HttpPost]
		[Route("SocialStream", RouteName = "SocialStreamFilter")]
		[BaseMembersModelAction]
		public ActionResult SocialStreamFilter(string filterParametersSocial)
		{
			ViewBag.filterParameters = filterParametersSocial;
			ViewBag.LocationName = "";

			return View("ReviewSites");
		}

		[BaseMembersModelAction("CanReputationStream")]
		public ActionResult ReviewStreamHeader()
		{
            //BaseMembersModel model = ViewBag.baseModel as BaseMembersModel;
            //ReputationOverview overview = ReputationService.ReviewStreamHeader(model.UserInfo);

			return PartialView("_ReviewStreamHeader");
		}

		[BaseMembersModelAction("CanReputationStream")]
		public JsonResult ReviewStreamHeaderCountPerSite([DataSourceRequest] DataSourceRequest request)
		{
			BaseMembersModel model = ViewBag.baseModel as BaseMembersModel;
			List<ReviewCountPerSite> overview = ReputationService.ReviewCountPerSite(model.UserInfo, KendoSupport.PagedGridRequest(request));

			return Json(overview);
		}

		[BaseMembersModelAction("CanReputationStream")]
		public JsonResult ReviewStreamHeaderPosNeg()
		{
			BaseMembersModel model = ViewBag.baseModel as BaseMembersModel;
			List<ReviewPosNegChart> posNegCharts = ReputationService.ReviewPosNegChart(model.UserInfo);

			return Json(posNegCharts);
		}

		public ActionResult ReviewAction(long? reviewID, string menuaction)
		{
			BaseMembersModel model = new BaseMembersModel();

			if (menuaction == "email")
			{
				ReviewEmail reviewEmail = ReputationService.ReviewEmail(model.UserInfo, reviewID.GetValueOrDefault());
				return PartialView("_ReviewEmail", reviewEmail);
			}
			
			if (menuaction == "share")
			{
				return PartialView("_ReviewShare"); //need design
			}

			//other actions, read, unread, responsed etc...
			bool Success = ReputationService.ReviewAction(model.UserInfo, reviewID.GetValueOrDefault(), menuaction);

			
			return Json(new { success = Success}, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public ActionResult ReviewActionEmail(ReviewEmail reviewEmail)
		{
			BaseMembersModel model = new BaseMembersModel();

			string modelErrors = "";
			bool success = false;

			if (ModelState.IsValid)
			{
				//send email
				List<string> Problems = new List<string>();
				success = ReputationService.ReviewActionEmail(model.UserInfo, reviewEmail, ref Problems);
				
				if (!success)
					modelErrors = SIHelpers.EntityErrorsToHtml(Problems);
			}
			else
				modelErrors = SIHelpers.ModelErrorsToHtml(ModelState);

			string returnStatus;
			if (success)
				returnStatus = string.Format("Saved on {0}", DateTime.Now.ToString());
			else
				returnStatus = string.Format("Unable to Save at this time. {0}", modelErrors);

			return Content(returnStatus);
		}

		
		public JsonResult ReviewStreamListGrid([DataSourceRequest] DataSourceRequest request)
		{			
            BaseMembersModel model = new BaseMembersModel();

            int TotalCount = 0;
			List<Stream> streams = ReputationService.ReviewStreamListGrid(model.UserInfo, KendoSupport.PagedGridRequest(request), ref TotalCount);

            DataSourceResult result = new DataSourceResult();
            result.Data = streams;
            result.Total = TotalCount;

            return Json(result, JsonRequestBehavior.AllowGet);
		}
    
		public JsonResult VirtualGroupList(string text)
		{
			BaseMembersModel model = new BaseMembersModel();

			string filter = "";
			if (!string.IsNullOrEmpty(text))
				filter = text.Trim().ToLower();
			
			List<SelectListItem> vGroups = ReputationService.VirtualGroupList(model.UserInfo, filter);
			
			return Json(vGroups, JsonRequestBehavior.AllowGet);
		}


		public JsonResult DealerList(string virtualGroup, string dealerFilter, long? dealerIDFilter)
		{
			BaseMembersModel model = new BaseMembersModel();

			List<SelectListItem> dealers = ReputationService.DealerList(virtualGroup, dealerFilter, model.UserInfo, dealerIDFilter.GetValueOrDefault());
			
			return Json(dealers, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ReviewSourceList()
		{
            BaseMembersModel model = new BaseMembersModel();
			return Json(ReputationService.GetReviewSources(model.UserInfo), JsonRequestBehavior.AllowGet);
		}

		public JsonResult CategoryList()
		{
			return Json(ReputationService.CategoryList(), JsonRequestBehavior.AllowGet);
		}

		public JsonResult StatesList()
		{
			return Json(ReputationService.StatesList(), JsonRequestBehavior.AllowGet);
		}

		public JsonResult OEMList()
		{
			return Json(ReputationService.OEMList(), JsonRequestBehavior.AllowGet);
		}

	

		public JsonResult ReviewSitesGrid([DataSourceRequest] DataSourceRequest request, string timeFrame)
		{
            BaseMembersModel model = new BaseMembersModel();

			//TODO build results off of timeFrame (counts) Add filtering
			List<Sites> sites = ReputationService.ReviewSitesGrid(model.UserInfo, KendoSupport.PagedGridRequest(request), timeFrame);
			
			return Json(sites.ToDataSourceResult(request));
		}

		[Route("CompetitiveAnalysisReport", RouteName = "ReputationCompetitiveAnalysisReport")]
		[BaseMembersModelAction("CanCompetitiveAnalysis")]
		public ActionResult CompetitiveAnalysis()
		{
			BaseMembersModel model = ViewBag.baseModel as BaseMembersModel;

			CompetitiveAnalysis competitiveAnalysis = ReputationService.CompetitiveAnalysisReport(model.UserInfo);

			return View(competitiveAnalysis);
		}



	}
}
