﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using RazorEngine;
using SI.Services.SupportCenter;
using SI.Services.SupportCenter.Models;
using SI.WEB.Portal.Framework;
using SI.WEB.Portal.Models;
using SI.WEB.Portal.PlatformTemplates.Models;

namespace SI.WEB.Portal.Areas.SupportCenter.Controllers
{
	[Authorize]
	[RouteArea("SupportCenter")]
    public class SupportCenterController : Controller
    {
        
        public ActionResult SupportCenter()
        {
			BaseMembersModel model = new BaseMembersModel();
			if (model.UserInfo.EffectiveUser.CanSupportCenter)
				return PartialView("_SupportCenter");

	        return null;
        }

		public ActionResult AccountManagers()
		{
			return PartialView("_AccountManagers");
		}

		public ActionResult Employees()
		{
			Employee employee = new Employee();
			employee.ID = 0;
 
			return PartialView("_Employees", employee);
		}

		[HttpPost]
		public ActionResult Employees(Employee employee)
		{
			string modelErrors = "";
			bool success = false;

			if (ModelState.IsValid)
			{
				//TODO Save
				//virtualGroupSave.SelectedLocationIDs  //comma delimited ids
				success = true;
			}
			else
				modelErrors = SIHelpers.ModelErrorsToHtml(ModelState);

			string returnStatus;

			if (success)
				returnStatus = string.Format("Saved on {0}", System.DateTime.Now.ToString());
			else
				returnStatus = "Unable to save at this time." + modelErrors;

			return Json(new { success, message = returnStatus });
		}

		[HttpPost]
		public ActionResult SendEmailInvitation(int? ID)
		{
			bool success = true;
			//success = //TODO SEND MESSAGE

			string returnStatus;

			//TODO custimize this message in the BLL??
			if (success)
				returnStatus = string.Format("Email Sent on {0}", System.DateTime.Now.ToString());
			else
				returnStatus = "Unable to send email at this time.";

			return Json(new { success, message = returnStatus });
		}

		public JsonResult EmployeeRoleTypeList()
		{
			List<SelectListItem> roles = SupportCenterService.GetRoleTypes();

			return Json(roles, JsonRequestBehavior.AllowGet);
		}

		public JsonResult EmployeeEditGrid([DataSourceRequest] DataSourceRequest request)
		{
			List<Employee> employees = SupportCenterService.EmployeeEditGrid(KendoSupport.PagedGridRequest(request));

			return Json(employees.ToDataSourceResult(request));
		}

		public ActionResult Packages()
		{
			return PartialView("_Packages");
		}

		public ActionResult PlatformMessages()
		{
			PlatformMessageConfig platformMessageConfig = new PlatformMessageConfig();

			return PartialView("_PlatformMessages", platformMessageConfig);
		}

		[HttpPost]
		public ActionResult PlatformMessages(PlatformMessageConfig platformMessageConfig)
		{
			string modelErrors = "";
			bool success = false;

			if (ModelState.IsValid)
			{
				//TODO Save 
				//TEMP FOR DEMO
				Session["demoValue"] = platformMessageConfig.ParameterDefinitions;
				success = true;
			}
			else
				modelErrors = SIHelpers.ModelErrorsToHtml(ModelState);

			string returnStatus;

			if (success)
				returnStatus = string.Format("Saved on {0}", System.DateTime.Now.ToString());
			else
				returnStatus = "Unable to save at this time." + modelErrors;

			return Json(new { success, message = returnStatus });
		}

		public JsonResult PlatformMessagesEditGrid([DataSourceRequest] DataSourceRequest request)
		{
			//can remove demovalue once implemented
			string demoValue = "";
			if (Session["demoValue"] != null)
				demoValue = Session["demoValue"].ToString();

			List<PlatformMessageConfig> configs = SupportCenterService.PlatformMessagesEditGrid(KendoSupport.PagedGridRequest(request), demoValue);

			return Json(configs.ToDataSourceResult(request));
		}

		public ActionResult GetTemplate(int? templateID)
		{
			Welcome welcome = new Welcome(Request.Url.OriginalString);
			welcome.Title = "Daves TEst of the template";
			welcome.Body = "<ul><li>this item one</li><li>this item two</li><li>this item three</li></ul>";

			if (Session["demoValue"] != null)
			{
				dynamic jsonvalues = Newtonsoft.Json.JsonConvert.DeserializeObject(Session["demoValue"].ToString());

				welcome.Title = jsonvalues.fields[0].value;
				welcome.Body = WebUtility.HtmlDecode((string)jsonvalues.fields[1].htmlValue);
			}

			string template = System.IO.File.ReadAllText(HttpContext.Server.MapPath("~/PlatformTemplates/Welcome.cshtml"));
			
			return Content(Razor.Parse(template, welcome));
		}

		public ActionResult SingleSignOn()
		{
			return PartialView("_SingleSignOn");
		}

		public ActionResult ThemesVertical()
		{
			return PartialView("_ThemesVertical");
		}

		

		public JsonResult ThemesVerticalEditGrid([DataSourceRequest] DataSourceRequest request)
		{
			List<ThemesVertical> themes = SupportCenterService.ThemesVerticalEditGrid(KendoSupport.PagedGridRequest(request));

			return Json(themes.ToDataSourceResult(request));
		}

		[HttpPost]
		public JsonResult ApplyTheme(int? themeID)
		{
			//TODO save theme in account
			return Json(new { success = true, message = "" });
		}

		public JsonResult PackagesEditGrid([DataSourceRequest] DataSourceRequest request)
		{
			List<Package> packages = SupportCenterService.PackagesEditGrid(KendoSupport.PagedGridRequest(request));

			return Json(packages.ToDataSourceResult(request));
		}
		
		[HttpPost]
		public JsonResult ManagePackage(int? packageID, string action)
		{
			//action = "Add" or "Remove"

			//TODO save package in account

			if (action == "Add")
				return Json(new { success = true, message = "" });
			else
				return Json(new { success = false, message = "Can not remove this Package. There are 4 Accounts using it." }); //Just an example :)
			
		}

    }
}
