﻿	
var ComposeMessage = {
	kendoValidator: null,
	images: "",
	previewHtml: "",
	SendDateTimer: null,
	uploadedImages: [],
	targetTypes: null,

	imageArray: [],
	imageIdx: 0,
	linkImageTimer: null,
	autoLinkTimerID: null,
	uploadPath: '/UploadedPostImages/',
	fullPageMode: false,
	userTimeZone: '',
	contentLibraryPost: false,
	

	submitCount: 0,
	BeginSubmit: function() {
		if (ComposeMessage.submitCount > 0) return false;
		ComposeMessage.submitCount++;
	},


	UploadSuccess: function(e) {
		if (e.operation == "upload" && e.response != "ERROR") {
			if (ComposeMessage.images != "")
				ComposeMessage.images += ",";
			ComposeMessage.images += e.response;

			ComposeMessage.previewHtml += "<img src='" + ComposeMessage.uploadPath + e.response + "' />";
			ComposeMessage.uploadedImages.push({ org: e.files[0].name, guid: e.response });

			if (ComposeMessage.uploadedImages.length > 1) {
				$(".many-images").show();
				MAfilterViewModel.networkFilterItems.push({ ID: "1", Name: "Facebook" });
				kendo.bind($("#MAFilterView"), MAfilterViewModel);
				MAfilterViewModel.applyNewFilters();
				$("#networkFilterItems_listbox").css("display", "none");
			}
		}
	},

	UploadRemove: function(e) {
		var initialImages = ComposeMessage.uploadedImages.length;
		var removeGuid = $.grep(ComposeMessage.uploadedImages, function(i) { return i.org == e.files[0].name; })[0].guid;
		ComposeMessage.uploadedImages = $.grep(ComposeMessage.uploadedImages, function(i) { return i.guid != removeGuid; });
		e.files[0].name = removeGuid;

		ComposeMessage.images = "";
		if (ComposeMessage.uploadedImages.length > 0) {
			for (var idx = 0; idx < ComposeMessage.uploadedImages.length; idx++) {
				if (ComposeMessage.images != "")
					ComposeMessage.images += ",";
				ComposeMessage.images += ComposeMessage.uploadedImages[idx].guid;
			}
		}
		$("#ImageUrl").val(ComposeMessage.images);
		ComposeMessage.previewHtml = ComposeMessage.previewHtml.replace("<img src='" + ComposeMessage.uploadPath + removeGuid + "' />", "");
		$("#UploadedImagePreview").html(ComposeMessage.previewHtml);
		if (ComposeMessage.images == "")
			$(".link-section").fadeOut();

		if (ComposeMessage.uploadedImages.length <= 1 && initialImages > 1) {
			$(".many-images").hide();
			MAfilterViewModel.networkFilterItems.length = 0;
			kendo.bind($("#MAFilterView"), MAfilterViewModel);
			MAfilterViewModel.applyNewFilters();
			$("#networkFilterItems_listbox").css("display", "block");
		}
	},

	UploadCompleted: function() {
		$("#UploadedImagePreview").html(ComposeMessage.previewHtml).show();
		$("#ImageUrl").val(ComposeMessage.images);
		$("#Previews").hide();
		$(".link-section").fadeIn();
		$("#LinkTitle").val("");
		$("#LinkMessage").val("");
		$("#LinkImage").attr("src", "");
		$("#LinkUrl").val("");
		$("#LinkImageUrl").val("");
		ComposeMessage.imageIdx = 0;
		$("#imageNum").html(0);
		$("#imageCount").html(0);
	},

	PreUpload: function(e) {
		$.each(e.files, function() {
			if (this.extension.toLowerCase() != ".jpg" && this.extension.toLowerCase() != ".png" && this.extension.toLowerCase() != ".jpeg") {
				SIGlobal.alertDialog("Image Upload", "Only .jpg and .png files can be uploaded.");
				e.preventDefault();
			}
		});
	},

	UploadError: function(e) {
		if (e.operation == "upload") {
			SIGlobal.alertDialog("Upload Error", "Failed to upload " + e.files[0].name, "e");
			$("#ImageUrl").val("");
			$("#UploadedImagePreview").html("");
		}
	},

	postTimeChange: function () {
		clearInterval(ComposeMessage.SendDateTimer);

		$("#SendTime").data("kendoTimePicker").min(new Date(2000, 0, 1, 6, 0, 0));

		//var ts = SIGlobal.DisplayTimeDelta(postDate);
		//$("#timeDelta").html(ts);
		var postDate = ComposeMessage.SetPostTimeDate();

		var approvalExpires = $("#ApprovalDate").data("kendoDatePicker");
		if (approvalExpires != null) {
			var onehour = new Date(postDate);
			onehour.add(4).week();
			//onehour.add(-1).day();

			if (approvalExpires.value() > onehour) {
				approvalExpires.value(onehour);
				$("#ApprovalTime").data("kendoTimePicker").value(onehour);
			}
			approvalExpires.max(onehour);
		}

		MessageAccounts.SetAccountTimeToPost(postDate);
	},

	SetPostTimeDate: function() {
		clearInterval(ComposeMessage.SendDateTimer);

		var postPicker = $("#PostDate").data("kendoDatePicker");
		var postDate = postPicker.value();

		var timePicker = $("#SendTime").data("kendoTimePicker");
		if (timePicker.value()) {
			postDate.set({ hour: timePicker.value().getHours(), minute: timePicker.value().getMinutes() })
			postPicker.value(postDate);
		}
		$("#SendDate").val(kendo.toString(postDate, 'MM/dd/yyyy hh:mm:ss tt'));

		return postDate;
	},

	approvalTimeChange: function(e) {
		ComposeMessage.SetApprovalTimeDate();
		//$("#ApprovalTime").data("kendoTimePicker").max(new Date(2100, 0, 1, 23, 0, 0));
	},

	SetApprovalTimeDate: function() {
		var approvalExpires = $("#ApprovalDate").data("kendoDatePicker");
		if (approvalExpires == null)
			return null;

		var type = $('input:radio[name=ApprovalDateType]:checked').val();
		if (type == "date") {
			var timePicker = $("#ApprovalTime").data("kendoTimePicker");
			var approvalDate = approvalExpires.value();

			if (timePicker.value()) {
				approvalDate.set({ hour: timePicker.value().getHours(), minute: timePicker.value().getMinutes() })
				approvalExpires.value(approvalDate);
			}

			var postDate = ComposeMessage.SetPostTimeDate();
			var compareDate = new Date(approvalDate);
			if (postDate < compareDate.add(1).hours()) {
				approvalDate.set({ hour: postDate.getHours() - 1, minute: postDate.getMinutes() })
				approvalExpires.value(approvalDate);
				timePicker.value(approvalDate);
			}
			$("#ApprovalExpires").val(kendo.toString(approvalDate, 'MM/dd/yyyy hh:mm:ss tt'));

			return approvalDate;
		} else {
			$("#ApprovalExpires").val("1/1/1900");
			return null;
		}
	},

	MessageSave: function(result) {
		$("#resultSave").html(result.message);
		ComposeMessage.submitCount = 0;
		if (result.success) {
			if (ComposeMessage.contentLibraryPost == "False")
				document.location.href = "/Social/PostActivity";
			else
				PostSaved("Last Post " + result.message);
		}
	},

	SaveFailure: function() {
		SIGlobal.formSaveFailure();
		ComposeMessage.submitCount = 0;
	},

	AllowPostNow: true,
	ForceApproval: true,
	MessageTypeChange: function(e) {
		var mType = this.dataSource.at(this.select());
		$("#SyndicatorID").val(mType.SyndicatorID)

		ComposeMessage.AllowPostNow = mType.AllowPostNow;
		ComposeMessage.ForceApproval = mType.ForceApproval;

		var accountsGrid = $('#MessageAccounts').data("kendoGrid");
		accountsGrid.dataSource.transport.options.read.url = "/Social/Social/MessageAccountsGrid?syndicatorID=" + mType.SyndicatorID;

		$("#SyndicationLogo").attr("src", "");
		if (mType.Logo != "") {
			$("#SyndicationLogo").attr("src", mType.Logo).show();
			accountsGrid.showColumn("IsAutoApprove");
		} else {
			accountsGrid.hideColumn("IsAutoApprove");
		}

		if (!ComposeMessage.AllowPostNow)
			$("#PostType_2").attr("disabled", "disabled");
		if (ComposeMessage.ForceApproval) {
			$("#IsRequiresApprovals").prop('checked', true).attr("disabled", "disabled");
		}

		MAfilterViewModel.applyNewFilters();
	},

	UpdateCounts: function() {
		var count = $("#Message").val().length;
		$(".message-count").html(count);
		var twitterOffset = $("#LinkUrl").val() != "" ? 23 : 0;
		var twiterCount = 140 - count - twitterOffset;
		$("#twitterRemaining").html(twiterCount);
		if (twiterCount < 0) {
			$("#twitterRemaining").css("color", "#af0000");
		} else {
			$("#twitterRemaining").css("color", "#555555");
		}
	},

	AutoLink: function() {
		var message = $("#Message").val();
		var httpIdx = message.indexOf("http://");
		if (httpIdx < 0)
			httpIdx = message.indexOf("https://");
		if (httpIdx >= 0) {
			var firstSpace = message.substring(httpIdx).indexOf(' ');
			if (firstSpace <= 0)
				firstSpace = message.substring(httpIdx).indexOf('\n');
			if (firstSpace > 0) {
				var snippet = message.substring(httpIdx, firstSpace + httpIdx);
				if ($("#LinkUrlDisplay").html() != snippet && ComposeMessage.ValidUrl(snippet)) {
					ComposeMessage.AutoLinkScrap(snippet);
				}
			}
		}
	},

	ValidUrl: function(str) {
		var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
			'((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
			'((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
			'(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
			'(\\?[;&a-z\\d%_.~+=/-]*)?' + // query string
			'(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
		return pattern.test(str);
	},

	AutoLinkScrap: function(url) {
		$("#LoadingPreviewIcon").show();
		$(".link-section").fadeIn();
		ComposeMessage.imageArray = [];

		$.get('/Social/Social/AutoLinkScrap?url=' + encodeURIComponent(url), function(data) {
			$("#LoadingPreviewIcon").hide();
			if (data.IsValidUrl) {
				$("#ImageUrl").val("");
				$("#UploadedImagePreview").html("");
				$("#LinkTitle").val(data.PageTitle);
				$("#LinkMessage").val(data.PageDescription);

				if (data.PageImages.length > 0) {
					$("#LinkImage").attr("src", data.PageImages[0]).load(function() {
						$("#ImagePlaceHolder").css("display", "none");
						$("#LinkImage").fadeIn();
					});
					$("#LinkImageUrl").val(data.PageImages[0]);
				}

				ComposeMessage.imageArray = data.PageImages;
				ComposeMessage.imageIdx = 0;
				$("#imageNum").html(1);
				$("#imageCount").html(data.PageImages.length);
				$("#LinkUrlDisplay").html(url);
				$("#LinkUrl").val(url);
				$("#Previews").fadeIn();
				ComposeMessage.UpdateCounts();
			} else {
				$("#LinkTitle").val("");
				$("#LinkMessage").val("");
				$("#LinkImage").attr("src", "");
				$("#LinkImageUrl").val("");
				ComposeMessage.imageIdx = 0;
				$("#imageNum").html(0);
				$("#imageCount").html(0);
				$("#LinkUrlDisplay").html("URL can not be resolved!");
				$(".link-section").fadeIn();
			}
		})
	},

	HasTwitterAccount: function() {
		var accounts = MessageAccounts.SelectedAccounts();
		for (var idx = 0; idx < accounts.length; idx++) {
			if (accounts[idx].Network == "twitter") return true;
		}
		return false;
	},

	TextSelect: function(obj) {
		ComposeMessage.TextDeSelect();
		if (document.selection) {
			var range = document.body.createTextRange();
			range.moveToElementText(obj);
			range.select();
		} else if (window.getSelection) {
			var range = document.createRange();
			range.selectNode(obj);
			window.getSelection().addRange(range);
		}
	},

	TextDeSelect: function() {
		if (document.selection) document.selection.empty();
		else if (window.getSelection)
			window.getSelection().removeAllRanges();
	},

	Init: function() {
		ComposeMessage.kendoValidator = $("#formComposeMessage").kendoValidator().data("kendoValidator");

		$("#Message").css("overflow", "hidden").autogrow();
		$("#Message").on("keyup", function() {
			ComposeMessage.UpdateCounts();

			if ($("#UploadedImagePreview").css("display") != "none") return;
			if (ComposeMessage.autoLinkTimerID)
				clearTimeout(ComposeMessage.autoLinkTimerID);
			ComposeMessage.autoLinkTimerID = setTimeout(ComposeMessage.AutoLink, 800);
		});

		SIGlobal.placeHolderSim(this);

		var sendTime = $("#SendTime").data("kendoTimePicker");
		if (sendTime != null) {
			sendTime.enable();
			$("#SendTime").attr("disabled", "disabled").attr("title", "Use Time Picker to change.");
		}
		
		var approvalTime = $("#ApprovalTime").data("kendoTimePicker")
		if (approvalTime != null) {
			approvalTime.enable();
			$("#ApprovalTime").attr("disabled", "disabled").attr("title", "Use Time Picker to change.");
		}

		$(".draggable").on("mousedown", function() {
			ComposeMessage.TextSelect(this);
		}).on("click", function() {
			$("#Message").val($("#Message").val() + " " + $(this).text() + " ");
		}).on("dragend", function() {
			ComposeMessage.TextDeSelect(this);
		});

		$("#leftImage").on("click", function(e) {
			e.preventDefault();
			if (ComposeMessage.imageIdx > 0) {
				ComposeMessage.imageIdx--;
				$("#LinkImage").fadeOut(function() {
					$("#LinkImage").attr("src", ComposeMessage.imageArray[ComposeMessage.imageIdx]);
					$("#LinkImage").fadeIn();
					$("#imageNum").html(ComposeMessage.imageIdx + 1);
					$("#LinkImageUrl").val(ComposeMessage.imageArray[ComposeMessage.imageIdx]);
				});
			}
			return false;
		})
		$("#rightImage").on("click", function(e) {
			e.preventDefault();
			if (ComposeMessage.imageIdx < ComposeMessage.imageArray.length - 1) {
				ComposeMessage.imageIdx++;
				$("#LinkImage").fadeOut(function() {
					$("#ImagePlaceHolder").css("display", "inline-block");
					$("#LinkImage").attr("src", ComposeMessage.imageArray[ComposeMessage.imageIdx]).load(function() {
						$("#ImagePlaceHolder").css("display", "none");
						$("#LinkImage").fadeIn();
					});
					$("#imageNum").html(ComposeMessage.imageIdx + 1);
					$("#LinkImageUrl").val(ComposeMessage.imageArray[ComposeMessage.imageIdx]);
				});
			}
			return false;
		})

		$("#AddLinkButton").on("click", function() {
			var addUrl = $("#LinkUrl").val();
			if (addUrl != "") {
				if (addUrl.indexOf("http://") < 0 && addUrl.indexOf("https://") < 0)
					addUrl = "http://" + addUrl;
				if ($("#LinkUrlDisplay").html() != addUrl && ComposeMessage.ValidUrl(addUrl)) {
					ComposeMessage.AutoLinkScrap(addUrl);
				}
			}
		});

		$("#LinkImageUrl").on("keyup paste", function() {
			if (ComposeMessage.linkImageTimer != null)
				clearTimeout(ComposeMessage.linkImageTimer);

			ComposeMessage.linkImageTimer = setTimeout(function() {
				if ($("#LinkImage").attr("src") != $("#LinkImageUrl").val()) {
					$("#LinkImage").fadeOut("fast", function() {
						$("#ImagePlaceHolder").css("display", "inline-block");
						$("#LinkImage").attr("src", $("#LinkImageUrl").val()).load(function() {
							$("#ImagePlaceHolder").css("display", "none");
							$("#LinkImage").fadeIn("fast");
						});
					});
				}

			}, 500);
		});

		$("#ResetButton").click(function() {
			ComposeMessage.images = "";
			ComposeMessage.previewHtml = "";
			$("#UploadedImagePreview").html("").hide();
			$("#ImageUrl").val("");
			$("#Title").val("");
			$("#Message").val("");
			$("#LinkUrl").val("");
			$("#LinkTitle").val("");
			$("#LinkMessage").val("");
			$("#LinkImage").attr("src", "");
			$("#LinkImageUrl").val("");
			ComposeMessage.imageIdx = 0;
			$("#imageNum").html(0);
			$("#imageCount").html(0);
			$("#LinkUrlDisplay").html("");
			MessageAccounts.ClearSelectedAccounts();
			$(".k-upload-files").remove();
			$(".k-upload-status").remove();
			$("#Previews").hide();
			$(".link-section").hide();
			$("#CategoryID").data("kendoDropDownList").value("");

			if ($("#PublishOptions_AllowChangeImage") != null) {
				$("#PublishOptions_AllowChangePublishDate").removeAttr("checked");
				$("#PublishOptions_AllowChangeImage").removeAttr("checked");
				$("#PublishOptions_RequireImage").removeAttr("checked");
				$("#PublishOptions_AllowChangeMessage").removeAttr("checked");
				$("#PublishOptions_RequireMessage").removeAttr("checked");
				$("#PublishOptions_AllowChangeTargets").removeAttr("checked");
				$("#PublishOptions_ForbidFacebookTargetChange").removeAttr("checked");
				$("#PublishOptions_ForbidTwitterTargetChange").removeAttr("checked");
				$("#PublishOptions_ForbidGoogleTargetChange").removeAttr("checked");
				$("#PublishOptions_AllowChangeLinkURL").removeAttr("checked");
				$("#PublishOptions_RequireLinkURL").removeAttr("checked");
				$("#PublishOptions_AllowChangeLinkText").removeAttr("checked");
				$("#PublishOptions_AllowChangeLinkThumb").removeAttr("checked");
			}
		});

		$("#PostButton").click(function () {
			var errors = [];
			var postDate = ComposeMessage.SetPostTimeDate();

			if ($("#IsSendNow").val() == "false") {
				var utcPost = new Date(postDate.toISOString());
				var utcNow = new Date(new Date().toISOString());
				if (utcPost < utcNow) {
					errors.push("Post Date can not be in the Past.");
					SIGlobal.ValidationSummaryPopulate(errors);
					return;
				}
			}

			if (ComposeMessage.kendoValidator.validate()) {
				SIGlobal.ValidationSummaryClear();
				$("#resultSave").html("");

				if (!MessageAccounts.isSelectAll) {
					var accounts = MessageAccounts.SelectedAccounts();
					if (accounts.length == 0) {
						SIGlobal.alertDialog("Accounts", "Please select one or more Accounts.");
						return;
					} else {
						//for (var idx = 0; idx < accounts.length; idx++) {
						//	var acc = accounts[idx];
						//	if (acc.UniquenessCount > 1) {
						//		usageCount = 0;
						//		for (var idxU = 0; idxU < accounts.length; idxU++) {
						//			var accU = accounts[idxU];
						//			if (accU.SocialNetworkUniqueID == acc.SocialNetworkUniqueID)
						//				usageCount++;
						//		}
						//		if (usageCount > 1) {
						//			errors.push("Accounts selected must not have duplicates. (" + acc.SocialNetworkScreenName + ")");
						//			SIGlobal.ValidationSummaryPopulate(errors);
						//			return;
						//		}
						//	}
						//}
					}

					var faceBook = 0, twitter = 0, google = 0;

					var targets = "";
					var autoApproved = 0;
					for (var idx = 0; idx < accounts.length; idx++) {
						var acc = accounts[idx];
						switch (acc.Network.toLowerCase()) {
						case "facebook":
							faceBook++;
							break;
						case "twitter":
							twitter++;
							break;
						case "google":
							google++;
							break;
						}

						if (targets != "")
							targets += ",";
						targets += acc.CredentialID;
						if (acc.IsAutoApprove)
							autoApproved++;
					}
					$("#Targets").val(targets);

					$("#postTotalNetworks").html(faceBook + twitter + google);
					$("#postFacebook").html(faceBook);
					$("#postTwitter").html(twitter);
					$("#postGoogle").html(google);
					$("#postAutoApprove").html(autoApproved);
					$("#postRequiresApproval").html(accounts.length - autoApproved);
				} else {
					$("#postTotalNetworks").html(MessageAccounts.AccountsCount());
					$("#networkSummary").hide();
					$("#selectedAllText").show();
					$("#AccountFilterJson").val(MessageAccounts.accountFilterJson);
				}

				$("#postTitle").html($("#Title").val());
				$("#postCategory").html($("#CategoryID").data("kendoDropDownList").text());
				$("#postMessage").html($("#Message").val().replace($("#LinkUrl").val(), ""));
				$("#postLink").html($("#LinkUrl").val());
				
				var postDate = ComposeMessage.SetPostTimeDate();
				var expireDate = ComposeMessage.SetApprovalTimeDate();
				$("#postApprovalExpire").html("");
				if (expireDate)
					$("#postApprovalExpire").html(kendo.toString(expireDate, 'dddd MMMM d, yyyy at hh:mm tt') + ' ' + ComposeMessage.userTimeZone);
				else
					$("#postApprovalExpire").html("No Approval Expiration Date");

				if ($("#IsSendNow").val() == "true") {
					$("#postDateTime").html("Immediate");
				} else {
					$("#postDateTime").html(kendo.toString(postDate, 'dddd MMMM d, yyyy at hh:mm tt') + ' ' + ComposeMessage.userTimeZone);
				}

				if ($("#IsRequiresApprovals").prop('checked')) {
					$("#ApprovalSummary").show();
				}

				var pcWindow = $("#PostConfirmationWindow").data("kendoWindow");
				if (!pcWindow) {
					$("#PostConfirmationWindow").kendoWindow({
						width: "600px",
						title: "Post Confirmation",
						modal: true,
						draggable: false,
						resizable: false
					});
					pcWindow = $("#PostConfirmationWindow").data("kendoWindow");
				}
				pcWindow.center();
				pcWindow.open();

			} else {
				SIGlobal.ValidationSummaryPopulate(ComposeMessage.kendoValidator.errors());
			}

		});

		$("#PostConfirmationButton").click(function() {
			$("#PostConfirmationWindow").data("kendoWindow").close();
			var message = $("#Message");
			message.val(message.val().replace($("#LinkUrl").val(), ""));
			if ($("#IsRequiresApprovals").prop('checked')) {
				$("#IsRequiresApprovals").removeAttr("disabled");
			}
			$("#formComposeMessage").submit();
		});

		$("#NoPostButton").click(function() {
			$("#PostConfirmationWindow").data("kendoWindow").close();
		});

		$("input[name='PostDateType']:radio").on("change", function(e) {
			$(".PostTypeLabel").css("font-weight", "normal");
			$(this).next('label').css("font-weight", "bold");

			if (this.value == "now") {
				$("#IsSendNow").val("true");
				$("#timeDelta").html("");
				MessageAccounts.SetAccountTimeToPost(null, true);
			} else {
				MessageAccounts.SetAccountTimeToPost($("#PostDate").data("kendoDatePicker").value());
				$("#IsSendNow").val("false");
			}
		});


		//???????????????????????????????
		$("#IsRequiresApprovals").click(function() {
			var checked = $(this).prop('checked');
			$("#ApprovalDate").data("kendoDateTimePicker").enable(checked);
			if (checked) {
				$("#PostType_2").attr("disabled", "disabled");
				$("#PostType_1").prop("checked", true);
				$("#SendNowLabel").css("color", "#CCC")
				var sendDate = $("#PostDate").data("kendoDatePicker");
				var nowPlus = $("#ApprovalDate").data("kendoDatePicker").value().add(1).hours();
				sendDate.min(nowPlus);
				sendDate.value(nowPlus);

			} else {
				$("#PostType_2").removeAttr("disabled");
				$("#SendNowLabel").css("color", "#003F59")
			}
		});

		if ($("#PublishOptions_AllowChangeImage") != null) {
			$("#PublishOptions_AllowChangeImage").click(function() {
				if ($(this).prop("checked"))
					$("#PublishOptions_RequireImage").prop("checked", true);
			})
			$("#PublishOptions_AllowChangeMessage").click(function () {
				if ($(this).prop("checked"))
					$("#PublishOptions_RequireMessage").prop("checked", true);
			})
			$("#PublishOptions_AllowChangeTargets").click(function () {
				if ($(this).prop("checked")) {
					$("#PublishOptions_ForbidFacebookTargetChange").prop("checked", true);
					$("#PublishOptions_ForbidTwitterTargetChange").prop("checked", true);
					$("#PublishOptions_ForbidGoogleTargetChange").prop("checked", true);
				}
			})
			$("#PublishOptions_AllowChangeLinkURL").click(function () {
				if ($(this).prop("checked")) {
					$("#PublishOptions_RequireLinkURL").prop("checked", true);
					$("#PublishOptions_AllowChangeLinkText").prop("checked", true);
					$("#PublishOptions_AllowChangeLinkThumb").prop("checked", true);
				}
			})
		}

		ComposeMessage.SendDateTimer = setInterval(function() {
			var sendTime = $("#SendTime").data("kendoTimePicker");
			if (sendTime != null) {
				if (sendTime.value() == null) {
					sendTime.value($("#PostDate").data("kendoDatePicker").value());
				}
				sendTime.value(sendTime.value().addMinutes(1));
			}

			var approvalTime = $("#ApprovalTime").data("kendoTimePicker");

			if (approvalTime != null) {
				if (approvalTime.value() == null)
					approvalTime.value($("#ApprovalDate").data("kendoDatePicker").value());
				approvalTime.value(approvalTime.value().addMinutes(1));
			}

		}, 60000);
		
		$(window).on('unload', function() {
			clearInterval(ComposeMessage.SendDateTimer);
		});

	}
}

