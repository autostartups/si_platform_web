﻿
var MessageAccounts = {
	selectedAccounts: [],
	accountsGrid: null,
	isSelectAll: false,
	accountFilterJson: '',
	countryID: '1',
	
	filtersInit: function(e) {
		var filterMenu = e.sender.thead.find("th[data-field='DealerLocation.StateAbrev']").data("kendoFilterMenu");
		if (filterMenu.form) 
			filterMenu.form.find("span.k-dropdown:first").css("display", "none");
	},

	accountDataBound: function(e) {
		MessageAccounts.accountsGrid = e.sender;
		
		$("#MessageAccounts tbody tr .checkbox").each(function () {
			var $me = $(this);
			var dataItem = MessageAccounts.accountsGrid.dataItem($me.closest('tr'));
			if ($.inArray(dataItem, MessageAccounts.selectedAccounts) != -1 || MessageAccounts.isSelectAll)
				$me.prop("checked", "checked");
			
			var targets = $("#Targets").val();
			if (targets != "") {
				var selects = targets.split(',');
				if ($.inArray(dataItem.CredentialID.toString(), selects) >= 0) {
					$me.prop("checked", "checked");
					if ($.inArray(dataItem, MessageAccounts.selectedAccounts) == -1)
						MessageAccounts.selectedAccounts.push(dataItem);
				}
			}

			$me.on("click", function(e) {
				e.stopPropagation();
				var chkbx = $(this);
				var dataItem = MessageAccounts.accountsGrid.dataItem(chkbx.closest('tr'));
				if (chkbx.is(':checked')) {
					MessageAccounts.selectedAccounts.push(dataItem);
				} else {
					MessageAccounts.selectedAccounts.splice($.inArray(dataItem, MessageAccounts.selectedAccounts), 1);
				}
				$("#MessageAccounts .checkbox-all").removeAttr("checked");
				$("#SelectAll").removeAttr("checked");
				$("#SelectedAllAccounts").val("false");

				if (MessageAccounts.isSelectAll) {
					var ct = MessageAccounts.accountsGrid.dataSource.data().length;
					var view = MessageAccounts.accountsGrid.dataSource.view();
					for(var idx = 0; idx < ct; idx++) {
						if (view[idx].CredentialID != dataItem.CredentialID)
							MessageAccounts.selectedAccounts.push(view[idx]);
					}
				}

				MessageAccounts.isSelectAll = false;
				

				MessageAccounts.AccountsCount();
			});
		});

		MessageAccounts.AccountsCount();

		$("#MessageAccounts tbody tr").each(function () {
			var $me = $(this);
			$me.on("click", function(e) {
				var chkbox = $(this).find(".checkbox");
				chkbox.trigger("click");
			});
			
		});
	},
	
	noProp: function(e) {
		if (!e)
			e = window.event;

		if (e.stopPropagation) {
			e.stopPropagation();
		}
		else {
			e.cancelBubble = true;
		}
	},
	
	getNetwork: function (network, socialNetworkPictureURL, socialNetworkURL) {

		var pictureUrl = "/assets/style/aristo/images/stripe1.png";
		if (socialNetworkPictureURL != "")
			pictureUrl = socialNetworkPictureURL;

		var templt = kendo.template($("#network-template").html());
		return templt({ Network: network, PictureURL: pictureUrl, SocialURL: socialNetworkURL });
	},
	
	getMALocation: function (dealerLoc, screenName, uniquenessCount) {
		var uniqColor = "#555";
		if (uniquenessCount > 1)
			uniqColor = "#900";

		var templt = kendo.template($("#ma-location-template").html());
		return templt({ DealerLocation: dealerLoc, ScreenName: screenName, uniqColor: uniqColor });
	},

	getAutoApprove: function(autoApprove) {
		if (autoApprove)
			return "<div class='ico ico-selected'></div>";
		return "";
	},

	SetSelectAll: function() {
		$("#MessageAccounts .checkbox-all, #SelectAll").click(function () {
			MessageAccounts.isSelectAll = $(this).is(':checked');
			if (MessageAccounts.isSelectAll) {
				$("#MessageAccounts .checkbox-all").prop("checked", "checked");
				$("#SelectAll").prop("checked", "checked");
				$("#SelectedAllAccounts").val("true");
			} else {
				$("#MessageAccounts .checkbox-all").removeAttr("checked");
				$("#SelectAll").removeAttr("checked");
				$("#SelectedAllAccounts").val("false");
			}
			
			$("#MessageAccounts tbody tr .checkbox").each(function () {
				if (MessageAccounts.isSelectAll)
					$(this).prop("checked", "checked");
				else
					$(this).removeAttr("checked");
			});
			if (!MessageAccounts.isSelectAll)
				$("#Targets").val("");
			
			MessageAccounts.AccountsCount();
		});
	},

	SelectedAccounts: function() {
		return MessageAccounts.selectedAccounts;
	},

	ClearSelectedAccounts: function() {
		while (MessageAccounts.selectedAccounts.length > 0) {
			MessageAccounts.selectedAccounts.pop();
		}
		$("#Targets").val("");
		
		$("#MessageAccounts tbody tr .checkbox").each(function() {
			var me = $(this);
			if (me.is(':checked')) {
				me.removeAttr("checked");
			}
		});
		MessageAccounts.AccountsCount();
	},

	SetAccountTimeToPost: function(postDate, setImmediate) {

		return;

		//var grid = $("#MessageAccounts").data("kendoGrid");
		//$("#MessageAccounts tbody tr .time-to-post").each(function() {
		//	var me = $(this);
		//	if (setImmediate != null) {
		//		me.html("Immediate");
		//		return;
		//	}

		//	var row = me.closest('tr');
		//	var dataItem = grid.dataItem(row);
		//	var offsetMins = dataItem.TZOffsetMinutes;
		//	if (offsetMins > 0) {
		//		me.html("Immediate");
		//	} else {
		//		var pDate = new Date(postDate);
		//		var ts = SIGlobal.DisplayTimeDelta(pDate.addMinutes(Math.abs(offsetMins)));
		//		me.html(ts);
		//		var diffMins = Math.round((Math.abs(pDate - Date.now().addMinutes(SIGlobal.userTZoffsetMinutes)) / 1000) / 60);
		//		//console.log(diffMins);
		//		dataItem.CalcOffsetMins = diffMins;

		//	}

		//});
	},

	DecTimeToPost: function(postDate) {
		$("#MessageAccounts tbody tr .time-to-post").each(function() {
			var me = $(this);
			var dataItem = MessageAccounts.accountsGrid.dataItem(me.closest('tr'));
			var pDate = new Date(postDate);
			var calcMins = dataItem.CalcOffsetMins - 1;
			var ts = SIGlobal.DisplayTimeDelta(pDate.addMinutes(calcMins));
			me.html(ts);
			dataItem.CalcOffsetMins = calcMins;
		});
	},
		
	AccountsCount: function () {
		var totalCount = MessageAccounts.selectedAccounts.length;
		if (MessageAccounts.isSelectAll)
			totalCount = MessageAccounts.accountsGrid.dataSource.total();

		$("#AccountsSelectedCount").html(totalCount);
		return totalCount;
	},
	
	StateFilter: function(e) {
		e.kendoDropDownList({
			dataTextField: "Name",
			dataValueField: "ID",
			dataSource: {
				transport: {
					read: {
						dataType: "json",
						url: '/Public/List?type=states&countryID=' + MessageAccounts.countryID
					}
				}
			}
		});


	}	
}

var AccountViewModel = {

	dealerNameFilterValue: "",

	networkFilterItems: [],
	networkFilterItemSource: new kendo.data.DataSource({
		transport: { read: { url: '/Admin/Admin/GetSocialNetworks', dataType: "json" } }
	}),

	oemFilterItems: [],
	oemFilterItemSource: new kendo.data.DataSource({
		transport: { read: { url: '/Reputation/Reputation/OEMList', dataType: "json" } }
	}),

	groupFilterValue: null,
	groupFilterItemSource: new kendo.data.DataSource({
		transport: {
			read: { url: '/Reputation/Reputation/VirtualGroupList', dataType: "json" },
			parameterMap: function (data, type) {
				if (data.filter)
					return { text: data.filter.filters[0].value };
				return { text: "" };
			}
		},
		serverFiltering: true,
	}),

	dealerCurrentContext: 0,
	dealerFilterValue: null,

	dealerFilterItemSource: new kendo.data.DataSource({
		transport: {
			read: { url: '/Reputation/Reputation/DealerList', dataType: "json" },
			parameterMap: function (data, type) {
				var groupValue = "All";
				if (MAfilterViewModel.groupFilterValue != null)
					groupValue = MAfilterViewModel.groupFilterValue.Text;
				if (data.filter)
					return { virtualGroup: groupValue, dealerFilter: data.filter.filters[0].value };
				else if (MAfilterViewModel.dealerCurrentContext > 0) {
					return { virtualGroup: groupValue, dealerFilter: "", dealerIDFilter: MAfilterViewModel.dealerCurrentContext };
				}
				return { virtualGroup: groupValue, dealerFilter: "" };
			}
		},
		serverFiltering: true,
	}),

	//Events
	clearFilters: function (e) {
		e.preventDefault();
		this.groupFilterValue = null;
		this.dealerFilterValue = null;
		this.oemFilterItems.length = 0;
		this.networkFilterItems.length = 0;
		this.dealerNameFilterValue = "";

		kendo.bind($("#MAFilterView"), MAfilterViewModel);
		this.applyNewFilters();
	},

	applyNewFilters: function (e) {
		var filterObj = [];

		if (this.networkFilterItems != null && this.networkFilterItems.length > 0) {
			if (this.networkFilterItems.length == 1) {
				filterObj.push({ field: "NetworkID", operator: "eq", value: this.networkFilterItems[0].ID });
			} else {
				var multiFilterObj = { logic: "or", filters: [] };
				for (var idx = 0; idx < this.networkFilterItems.length; idx++) {
					multiFilterObj.filters.push({ field: "NetworkID", operator: "eq", value: this.networkFilterItems[idx].ID });
				}
				filterObj.push(multiFilterObj);
			}
		}

		if (this.oemFilterItems != null && this.oemFilterItems.length > 0) {
			if (this.oemFilterItems.length == 1) {
				filterObj.push({ field: "DealerLocation.OemID", operator: "eq", value: this.oemFilterItems[0].ID });
			} else {
				var multiFilterObj = { logic: "or", filters: [] };
				for (var idx = 0; idx < this.oemFilterItems.length; idx++) {
					multiFilterObj.filters.push({ field: "DealerLocation.OemID", operator: "eq", value: this.oemFilterItems[idx].ID });
				}
				filterObj.push(multiFilterObj);
			}
		}

		if (this.dealerFilterValue != null) {
			filterObj.push({ field: "DealerLocation.LocationID", operator: "eq", value: parseInt(this.dealerFilterValue.Value) });
		}
		if (this.groupFilterValue != null && this.groupFilterValue.Text != "All") {
			filterObj.push({ field: "DealerLocation.VirtualGroupID", operator: "eq", value: parseInt(this.groupFilterValue.Value) });
		}
		if (this.dealerNameFilterValue != null && this.dealerNameFilterValue != "") {
			filterObj.push({ field: "DealerLocation.Name", operator: "startswith", value: this.dealerNameFilterValue });
		}

		$('#MessageAccounts').data("kendoGrid").dataSource.filter(filterObj);
		MessageAccounts.accountFilterJson = JSON.stringify(filterObj);
	},

	searchSelect: function (e) {
		this.dealerNameFilterValue = e.target.value
		this.applyNewFilters();
	},


};



