﻿
var RevisePost = {
	kendoValidator: null,
	
	isMessageRequired: false,
	isImageRequired: false,
	isLinkURLRequired: false,
	canChangeTargets: true,
	
	images: "",
	previewHtml: "",
	SendDateTimer: null,
	uploadedImages: [],
	targetTypes: null,

	imageArray: [],
	imageIdx: 0,
	linkImageTimer: null,
	autoLinkTimerID: null,
	uploadPath: '/UploadedPostImages/',

	submitCount: 0,
	BeginSubmit: function () {
		if (RevisePost.submitCount > 0) return false;
		RevisePost.submitCount++;
	},

	UploadSuccess: function (e) {
		if (e.operation == "upload" && e.response != "ERROR") {
			if (RevisePost.images != "")
				RevisePost.images += ",";
			RevisePost.images += e.response;

			RevisePost.previewHtml += "<img src='" + RevisePost.uploadPath + e.response + "' />";
			RevisePost.uploadedImages.push({ org: e.files[0].name, guid: e.response });

			if (RevisePost.uploadedImages.length > 1) {
				$(".many-images").show();
				MAfilterViewModel.networkFilterItems.push({ ID: "1", Name: "Facebook" });
				kendo.bind($("#MAFilterView"), MAfilterViewModel);
				MAfilterViewModel.applyNewFilters();
				$("#networkFilterItems_listbox").css("display", "none");
			}
		}
	},

	UploadRemove: function (e) {
		var initialImages = RevisePost.uploadedImages.length;
		var removeGuid = $.grep(RevisePost.uploadedImages, function (i) { return i.org == e.files[0].name; })[0].guid;
		RevisePost.uploadedImages = $.grep(RevisePost.uploadedImages, function (i) { return i.guid != removeGuid; });
		e.files[0].name = removeGuid;

		RevisePost.images = "";
		if (RevisePost.uploadedImages.length > 0) {
			for (var idx = 0; idx < RevisePost.uploadedImages.length; idx++) {
				if (RevisePost.images != "")
					RevisePost.images += ",";
				RevisePost.images += RevisePost.uploadedImages[idx].guid;
			}
		}
		$("#ImageUrl").val(RevisePost.images);
		RevisePost.previewHtml = RevisePost.previewHtml.replace("<img src='" + RevisePost.uploadPath + removeGuid + "' />", "");
		$("#UploadedImagePreview").html(RevisePost.previewHtml);
		if (RevisePost.images == "")
			$(".link-section").fadeOut();

		if (RevisePost.uploadedImages.length <= 1 && initialImages > 1) {
			$(".many-images").hide();
			MAfilterViewModel.networkFilterItems.length = 0;
			kendo.bind($("#MAFilterView"), MAfilterViewModel);
			MAfilterViewModel.applyNewFilters();
			$("#networkFilterItems_listbox").css("display", "block");
		}
	},

	UploadCompleted: function () {
		$("#UploadedImagePreview").html(RevisePost.previewHtml).show();
		$("#ImageUrl").val(RevisePost.images);
		$("#Previews").hide();
		$(".link-section").fadeIn();
		$("#LinkTitle").val("");
		$("#LinkMessage").val("");
		$("#LinkImage").attr("src", "");
		$("#LinkUrl").val("");
		$("#LinkImageUrl").val("");
		RevisePost.imageIdx = 0;
		$("#imageNum").html(0);
		$("#imageCount").html(0);
	},

	PreUpload: function (e) {
		$.each(e.files, function () {
			if (this.extension.toLowerCase() != ".jpg" && this.extension.toLowerCase() != ".png" && this.extension.toLowerCase() != ".jpeg") {
				SIGlobal.alertDialog("Image Upload", "Only .jpg and .png files can be uploaded.");
				e.preventDefault();
			}
		});
	},

	UploadError: function (e) {
		if (e.operation == "upload") {
			SIGlobal.alertDialog("Upload Error", "Failed to upload " + e.files[0].name, "e");
			$("#ImageUrl").val("");
			$("#UploadedImagePreview").html("");
		}
	},

	postTimeChange: function () {
		clearInterval(RevisePost.SendDateTimer);

		$("#SendTime").data("kendoTimePicker").min(new Date(2000, 0, 1, 6, 0, 0));

		//var ts = SIGlobal.DisplayTimeDelta(postDate);
		//$("#timeDelta").html(ts);
		//var postDate = RevisePost.SetPostTimeDate();

		//MessageAccounts.SetAccountTimeToPost(postDate);
	},

	SetPostTimeDate: function () {
		clearInterval(RevisePost.SendDateTimer);

		var postPicker = $("#PostDate").data("kendoDatePicker");
		var postDate = postPicker.value();

		var timePicker = $("#SendTime").data("kendoTimePicker");
		if (timePicker.value()) {
			postDate.set({ hour: timePicker.value().getHours(), minute: timePicker.value().getMinutes() })
			postPicker.value(postDate);
		}
		$("#SendDate").val(kendo.toString(postDate, 'MM/dd/yyyy hh:mm:ss tt'));

		return postDate;
	},
	
	MessageSave: function (result) {
		$("#resultSave").html(result.message);
		RevisePost.submitCount = 0;
		if (result.success) {
			PostSaved("Last Post " + result.message);
		}
	},

	SaveFailure: function () {
		SIGlobal.formSaveFailure();
		RevisePost.submitCount = 0;
	},
	
	UpdateCounts: function () {
		var count = $("#Message").val().length;
		$(".message-count").html(count);
		var twitterOffset = $("#LinkUrl").val() != "" ? 23 : 0;
		var twiterCount = 140 - count - twitterOffset;
		$("#twitterRemaining").html(twiterCount);
		if (twiterCount < 0) {
			$("#twitterRemaining").css("color", "#af0000");
		} else {
			$("#twitterRemaining").css("color", "#555555");
		}
	},

	AutoLink: function () {
		var message = $("#Message").val();
		var httpIdx = message.indexOf("http://");
		if (httpIdx < 0)
			httpIdx = message.indexOf("https://");
		if (httpIdx >= 0) {
			var firstSpace = message.substring(httpIdx).indexOf(' ');
			if (firstSpace <= 0)
				firstSpace = message.substring(httpIdx).indexOf('\n');
			if (firstSpace > 0) {
				var snippet = message.substring(httpIdx, firstSpace + httpIdx);
				if ($("#LinkUrlDisplay").html() != snippet && RevisePost.ValidUrl(snippet)) {
					RevisePost.AutoLinkScrap(snippet);
				}
			}
		}
	},

	ValidUrl: function (str) {
		var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
			'((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
			'((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
			'(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
			'(\\?[;&a-z\\d%_.~+=/-]*)?' + // query string
			'(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
		return pattern.test(str);
	},

	AutoLinkScrap: function (url, forced) {
		forced = typeof forced !== 'undefined' ? forced : false;
		
		$("#LoadingPreviewIcon").show();
		$(".link-section").fadeIn();
		RevisePost.imageArray = [];

		$.get('/Social/Social/AutoLinkScrap?url=' + encodeURIComponent(url), function (data) {
			$("#LoadingPreviewIcon").hide();
			if (data.IsValidUrl) {
				$("#ImageUrl").val("");
				$("#UploadedImagePreview").html("");

				if (data.PageImages.length > 0) {
					$("#LinkImage").attr("src", data.PageImages[0]).load(function () {
						$("#ImagePlaceHolder").css("display", "none");
						$("#LinkImage").fadeIn();
					});
					$("#LinkImageUrl").val(data.PageImages[0]);
				}

				RevisePost.imageArray = data.PageImages;
				RevisePost.imageIdx = 0;
				$("#imageNum").html(1);
				$("#imageCount").html(data.PageImages.length);

				if (forced == false) {
					$("#LinkTitle").val(data.PageTitle);
					$("#LinkMessage").val(data.PageDescription);
					$("#LinkUrl").val(url);
				}
				
				$("#LinkUrlDisplay").html(url);
				$("#Previews").fadeIn();
				RevisePost.UpdateCounts();
			} else {
				$("#LinkTitle").val("");
				$("#LinkMessage").val("");
				$("#LinkImage").attr("src", "");
				$("#LinkImageUrl").val("");
				RevisePost.imageIdx = 0;
				$("#imageNum").html(0);
				$("#imageCount").html(0);
				$("#LinkUrlDisplay").html("URL can not be resolved!");
				$(".link-section").fadeIn();
			}
		})
	},
	
	Init: function (imageIdx, uploadPath, isMessageRequired, isImageRequired, isLinkURLRequired, canChangeTargets) {
		RevisePost.imageIdx = parseInt(imageIdx);
		RevisePost.uploadPath = uploadPath;
		RevisePost.isMessageRequired = isMessageRequired == 'true';
		RevisePost.isImageRequired = isImageRequired == 'true';
		RevisePost.isLinkURLRequired = isLinkURLRequired == 'true';
		RevisePost.canChangeTargets = canChangeTargets == 'true';

		RevisePost.kendoValidator = $("#formRevisePost").kendoValidator().data("kendoValidator");

		$("#Message").css("overflow", "hidden").autogrow();
		$("#Message").on("keyup", function () {
			RevisePost.UpdateCounts();

			if ($("#UploadedImagePreview").css("display") != "none") return;
			if (RevisePost.autoLinkTimerID)
				clearTimeout(RevisePost.autoLinkTimerID);
			RevisePost.autoLinkTimerID = setTimeout(RevisePost.AutoLink, 800);
		});
		RevisePost.UpdateCounts();

		SIGlobal.placeHolderSim(this);

		$("#SendTime").data("kendoTimePicker").enable();
		$("#SendTime").attr("disabled", "disabled").attr("title", "Use Time Picker to change.");
		
		$("#leftImage").on("click", function (e) {
			e.preventDefault();
			if (RevisePost.imageIdx > 0) {
				RevisePost.imageIdx--;
				$("#LinkImage").fadeOut(function () {
					$("#LinkImage").attr("src", RevisePost.imageArray[RevisePost.imageIdx]);
					$("#LinkImage").fadeIn();
					$("#imageNum").html(RevisePost.imageIdx + 1);
					$("#LinkImageUrl").val(RevisePost.imageArray[RevisePost.imageIdx]);
				});
			}
			return false;
		})
		$("#rightImage").on("click", function (e) {
			e.preventDefault();
			if (RevisePost.imageIdx < RevisePost.imageArray.length - 1) {
				RevisePost.imageIdx++;
				$("#LinkImage").fadeOut(function () {
					$("#ImagePlaceHolder").css("display", "inline-block");
					$("#LinkImage").attr("src", RevisePost.imageArray[RevisePost.imageIdx]).load(function () {
						$("#ImagePlaceHolder").css("display", "none");
						$("#LinkImage").fadeIn();
					});
					$("#imageNum").html(RevisePost.imageIdx + 1);
					$("#LinkImageUrl").val(RevisePost.imageArray[RevisePost.imageIdx]);
				});
			}
			return false;
		})

		$("#AddLinkButton").on("click", function () {
			var addUrl = $("#LinkUrl").val();
			if (addUrl != "") {
				if (addUrl.indexOf("http://") < 0 && addUrl.indexOf("https://") < 0)
					addUrl = "http://" + addUrl;
				if ($("#LinkUrlDisplay").html() != addUrl && RevisePost.ValidUrl(addUrl)) {
					RevisePost.AutoLinkScrap(addUrl);
				}
			}
		});

		$("#LinkImageUrl").on("keyup paste", function () {
			if (RevisePost.linkImageTimer != null)
				clearTimeout(RevisePost.linkImageTimer);

			RevisePost.linkImageTimer = setTimeout(function () {
				if ($("#LinkImage").attr("src") != $("#LinkImageUrl").val()) {
					$("#LinkImage").fadeOut("fast", function () {
						$("#ImagePlaceHolder").css("display", "inline-block");
						$("#LinkImage").attr("src", $("#LinkImageUrl").val()).load(function () {
							$("#ImagePlaceHolder").css("display", "none");
							$("#LinkImage").fadeIn("fast");
						});
					});
				}

			}, 500);
		});

		$("#ResetButton").click(function () {
			RevisePost.images = "";
			RevisePost.previewHtml = "";
			$("#UploadedImagePreview").html("").hide();
			$("#ImageUrl").val("");
			
			$("#Message").val("");
			$("#LinkUrl").val("");
			$("#LinkTitle").val("");
			$("#LinkMessage").val("");
			$("#LinkImage").attr("src", "");
			$("#LinkImageUrl").val("");
			RevisePost.imageIdx = 0;
			$("#imageNum").html(0);
			$("#imageCount").html(0);
			$("#LinkUrlDisplay").html("");
			
			$(".k-upload-files").remove();
			$(".k-upload-status").remove();
			$("#Previews").hide();
			$(".link-section").hide();
			
		});

		$("#PostButton").click(function () {
			var errors = [];
			//var postDate = RevisePost.SetPostTimeDate();

			//var utcPost = new Date(postDate.toISOString()); //the post can have a past date but not approved, with no rights to edit the post date
			//var utcNow = new Date(new Date().toISOString());
			//if (utcPost < utcNow) 
			//	errors.push("Post Date can not be in the Past.");
			if (RevisePost.isMessageRequired && $("#Message").val().trim() == "") 
				errors.push("Message is required.");
			if (RevisePost.isImageRequired && $("#ImageUrl").val().trim() == "")
				errors.push("Image is required.");
			if (RevisePost.isLinkURLRequired && $("#LinkUrl").val().trim() == "")
				errors.push("Link is required.");

			if (RevisePost.canChangeTargets) {
				var targets = "";
				$(".targetnetwork").each(function() {
					var $me = $(this);
					if ($me.prop("checked")) {
						if (targets != "") targets += ",";
						targets += $me.val();
					}
				})
				$("#TargetNetworks").val(targets);

				if (targets == "")
					errors.push("Social Network is required.");
			}

			if (errors.length > 0) {
				SIGlobal.ValidationSummaryPopulate(errors);
				return;
			}
			
			if (RevisePost.kendoValidator.validate()) {
				SIGlobal.ValidationSummaryClear();
				$("#resultSave").html("");

				RevisePost.SetPostTimeDate();
				var message = $("#Message");
				message.val(message.val().replace($("#LinkUrl").val(), ""));
				
				$("#formRevisePost").submit();

			} else {
				SIGlobal.ValidationSummaryPopulate(RevisePost.kendoValidator.errors());
			}

		});
		
		RevisePost.SendDateTimer = setInterval(function () {
			var sendTime = $("#SendTime").data("kendoTimePicker");
			if (sendTime.value() == null) {
				sendTime.value($("#PostDate").data("kendoDatePicker").value());
			}
			sendTime.value(sendTime.value().addMinutes(1));

		}, 60000);

		$(window).on('unload', function () {
			clearInterval(RevisePost.SendDateTimer);
		});

	}
}

