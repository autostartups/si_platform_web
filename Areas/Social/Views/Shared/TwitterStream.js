﻿

	
var TwitterStream = {
	selectedAccountID: 0,
	homeSince: "",
	userSince: "",
	mentionsSince: "",
	tweetHomeDataSource: null,
	tweetUserDataSource: null,
	tweetMentionsDataSource: null,
		
	ClearTweets: function() {
		$("#HomeTweets .tweet").remove();
		$("#UserTweets .tweet").remove();
		$("#MentionsTweets .tweet").remove();
	},
		
	LoadAccount: function (accountID) {
		$("#More").hide();
		TwitterStream.selectedAccountID = accountID;
		TwitterStream.since = "";
		TwitterStream.tweetHomeDataSource.transport.options.read.data.accountID = accountID;
		TwitterStream.tweetHomeDataSource.transport.options.read.data.until = TwitterStream.homeSince;
		TwitterStream.tweetUserDataSource.transport.options.read.data.accountID = accountID;
		TwitterStream.tweetUserDataSource.transport.options.read.data.until = TwitterStream.userSince;
		TwitterStream.tweetMentionsDataSource.transport.options.read.data.accountID = accountID;
		TwitterStream.tweetMentionsDataSource.transport.options.read.data.until = TwitterStream.mentionsSince;
		TwitterStream.tweetHomeDataSource.read();
		TwitterStream.tweetUserDataSource.read();
		TwitterStream.tweetMentionsDataSource.read();
	},

	TweetFavorite: function (id, favorite) {
		var tweet = $("#tweet" + id);
		kendo.ui.progress(tweet, true);
			
		var url = '/Social/Social/TweetFavorite';
		$.post(url, { tweetID: id, credentialID: tweet.data("credid"), favorite: favorite }, function (data) {
			kendo.ui.progress(tweet, false);
			if (!data.success)
				SIGlobal.alertDialog("Tweet Favorite", data.message, "e");
			else {
				var link = $("#tweet" + id + " .favorite-link");
				link.data("favorite", !favorite);
				linkIcon = link.find("span");
				if (!favorite)
					linkIcon.removeClass("ico-twitterfavorite").addClass("ico-twitterfavorited");
				else
					linkIcon.removeClass("ico-twitterfavorited").addClass("ico-twitterfavorite");
			}
		});
	},
		
	TweetDelete: function (id) {
		SIGlobal.confirmDialog("Delete Tweet", "Are you sure you want to Delete the Tweet?", function(button) {
			if (button == "Yes") {
				var tweet = $("#tweet" + id);
				kendo.ui.progress(tweet, true);

				var url = '/Social/Social/TweetDelete';
				$.post(url, { tweetID: id, credentialID: tweet.data("credid") }, function (data) {
					kendo.ui.progress(tweet, false);
					if (!data.success)
						SIGlobal.alertDialog("Tweet Delete", data.message, "e");
					else {
						tweet.remove();
					}
				});
			}
		});
	},
		
	TweetReply: function(id) {
		var tweet = $("#tweet" + id);
		var text = $(".text", tweet);
		var ats = [];
		$("a", text).each(function (i, val) {
			var at = $(val).html();
			if (at.indexOf("#") == -1 && at.indexOf("http://") == -1)
				ats.push("@@" + at);
		});
		var reply = "@@" + tweet.data("screenname") + " " + ats.join(" ") + " ";

		var tweetReply = $("#ReplyTweet" + id);
		if (tweetReply.html() == "") {
			var templt = kendo.template($("#tweetReplyTemplate").html());
			tweetReply.html(templt({ ID: id, reply: reply }));

			var replyText = $("#reply" + id).focus(function() {
				this.selectionStart = this.selectionEnd = this.value.length;
			}).on("keyup", function() {
				TwitterStream.UpdateCounts($(this));
			}).on("paste", function () {
				var self = $(this);
				setTimeout(function() {
					TwitterStream.UpdateCounts(self);
				}, 100);
			}).autogrow();
			TwitterStream.UpdateCounts(replyText);
				
			$(".tweet-save", tweetReply).click(function () {
				var id = $(this).data("id");
				var tweetme = $("#tweet" + id);
				kendo.ui.progress(tweetme, true);
				var url = '/Social/Social/TweetReply';
				$.post(url, { tweetID: id, credentialID: tweetme.data("credid"), reply: $("#reply" + id).val() }, function (data) {
					kendo.ui.progress(tweetme, false);
					if (!data.success)
						SIGlobal.alertDialog("Tweet Reply", data.message, "e");
					else {
						SIGlobal.alertDialog("Tweet Reply", "Your reply has been posted.", "i");
					}
					$("#ReplyTweet" + id).slideToggle("fast");
				});
			});
		} else {
			$("textarea", tweetReply).val(reply);
		}
		tweetReply.slideToggle("fast", function() {
			$("textarea", tweetReply).focus();
		});
	},
		
	UpdateCounts: function (me) {
		var text = me.val();
		var count = 140 - text.length;
			
		if (text.indexOf("http://") >= 0 || text.indexOf("https://") >= 0) {
			var words = text.split(" ");
			for (var idx = 0; idx < words.length; idx++) {
				if (words[idx].indexOf("http://") >= 0 || words[idx].indexOf("https://") >= 0) {
					count = (count + words[idx].length) - 23;
					break;
				}
			}
		}
			
		var id = me.data("id");
		var countspan = $("#CharCount" + id);
		countspan.html(count);
		countspan.css("color", count <= 10 ? "#af0000" : "#292F33");
	},
		
	dataBoundHome: function(e) {
		kendo.ui.progress($("#HomeTweets"), false);
		if (e.response != null && e.response.length > 0) {
			$("#HomeTweetsMore").show();
			//TwitterStream.homeSince = e.response[0].Since;

			setTimeout(function() {
				$("#HomeTweets .favorite-link").click(function () {
					var me = $(this);
					TwitterStream.TweetFavorite(me.data("id"), me.data("favorite"));
				});
					
				$("#HomeTweets .reply-link").click(function () {
					TwitterStream.TweetReply($(this).data("id"));
				});

			}, 100);
		}
	},
		
	dataBoundUser: function(e) {
		kendo.ui.progress($("#UserTweets"), false);
		if (e.response != null && e.response.length > 0) {
			$("#UserTweetsMore").show();
			//TwitterStream.userSince = e.response[0].Since;
				
			setTimeout(function () {
				$("#UserTweets .delete-link").click(function () {
					TwitterStream.TweetDelete($(this).data("id"));
				});
					
				$("#UserTweets .reply-link").click(function () {
					TwitterStream.TweetReply($(this).data("id"));
				});

			}, 100);
		}
	},

	dataBoundMentions: function (e) {
		kendo.ui.progress($("#MentionsTweets"), false);
		if (e.response != null && e.response.length > 0) {
			$("#MentionsTweetsMore").show();
			//TwitterStream.mentionsSince = e.response[0].Since;
				
			setTimeout(function () {
				$("#MentionsTweets .favorite-link").click(function () {
					var me = $(this);
					TwitterStream.TweetFavorite(me.data("id"),  me.data("favorite"));
				});
				$("#MentionsTweets .reply-link").click(function () {
					TwitterStream.TweetReply($(this).data("id"));
				});

			}, 100);
		}
	}
	
}

$(function() {
	var template = kendo.template($("#tweetTemplate").html());

	TwitterStream.tweetHomeDataSource = new kendo.data.DataSource({
		transport: {
			read: {
				url: '/Social/Social/TweetStreamListView',
				dataType: "json",
				data: { since: "", accountID: TwitterStream.selectedAccountID, section: "home", count: 0 }
			}
		},
		requestStart: function() {
			kendo.ui.progress($("#HomeTweets"), true);
		},
		requestEnd: TwitterStream.dataBoundHome,
		change: function() {
			$("#HomeTweets").append(kendo.render(template, this.view()));
		}
	});

	if (treeSelectedAccount > 0) {
		TwitterStream.selectedAccountID = treeSelectedAccount;
		TwitterStream.tweetHomeDataSource.transport.options.read.data.accountID = treeSelectedAccount;
		TwitterStream.tweetHomeDataSource.read();
	}

	TwitterStream.tweetUserDataSource = new kendo.data.DataSource({
		transport: {
			read: {
				url: '/Social/Social/TweetStreamListView',
				dataType: "json",
				data: { since: "", accountID: TwitterStream.selectedAccountID, section: "user", count: 0 }
			}
		},
		requestStart: function() {
			kendo.ui.progress($("#UserTweets"), true);
		},
		requestEnd: TwitterStream.dataBoundUser,
		change: function() {
			$("#UserTweets").append(kendo.render(template, this.view()));
		}
	});

	if (treeSelectedAccount > 0) {
		TwitterStream.tweetUserDataSource.transport.options.read.data.accountID = treeSelectedAccount;
		TwitterStream.tweetUserDataSource.read();
	}

	TwitterStream.tweetMentionsDataSource = new kendo.data.DataSource({
		transport: {
			read: {
				url: '/Social/Social/TweetStreamListView',
				dataType: "json",
				data: { since: "", accountID: TwitterStream.selectedAccountID, section: "mentions", count: 0 }
			}
		},
		requestStart: function() {
			kendo.ui.progress($("#MentionsTweets"), true);
		},
		requestEnd: TwitterStream.dataBoundMentions,
		change: function() {
			$("#MentionsTweets").append(kendo.render(template, this.view()));
		}
	});

	if (treeSelectedAccount > 0) {
		TwitterStream.tweetMentionsDataSource.transport.options.read.data.accountID = treeSelectedAccount;
		TwitterStream.tweetMentionsDataSource.read();
	}

	$("#HomeTweetsMore").click(function() {
		kendo.ui.progress($("#HomeTweets"), true);
		TwitterStream.tweetHomeDataSource.transport.options.read.data.since = TwitterStream.homeSince;
		TwitterStream.tweetHomeDataSource.transport.options.read.data.count = TwitterStream.tweetHomeDataSource.transport.options.read.data.count + 25;
		TwitterStream.tweetHomeDataSource.read();
	});
		
	$("#UserTweetsMore").click(function () {
		kendo.ui.progress($("#UserTweets"), true);
		TwitterStream.tweetUserDataSource.transport.options.read.data.since = TwitterStream.userSince;
		TwitterStream.tweetUserDataSource.transport.options.read.data.count = TwitterStream.tweetUserDataSource.transport.options.read.data.count + 25;
		TwitterStream.tweetUserDataSource.read();
	});
		
	$("#MentionsTweetsMore").click(function () {
		kendo.ui.progress($("#MentionsTweets"), true);
		TwitterStream.tweetMentionsDataSource.transport.options.read.data.since = TwitterStream.mentionsSince;
		TwitterStream.tweetMentionsDataSource.transport.options.read.data.count = TwitterStream.tweetMentionsDataSource.transport.options.read.data.count + 25;
		TwitterStream.tweetMentionsDataSource.read();
	});

	$("#HomeRefresh").click(function() {
		kendo.ui.progress($("#HomeTweets"), true);
		TwitterStream.homeSince = "";
		TwitterStream.tweetHomeDataSource.transport.options.read.data.since = TwitterStream.homeSince;
		TwitterStream.tweetHomeDataSource.transport.options.read.data.count = 0;
		$("#HomeTweets .tweet").remove();
		TwitterStream.tweetHomeDataSource.read();
	});
		
	$("#UserRefresh").click(function () {
		kendo.ui.progress($("#UserTweets"), true);
		TwitterStream.userSince = "";
		TwitterStream.tweetUserDataSource.transport.options.read.data.since = TwitterStream.userSince;
		TwitterStream.tweetUserDataSource.transport.options.read.data.count = 0;
		$("#UserTweets .tweet").remove();
		TwitterStream.tweetUserDataSource.read();
	});
		
	$("#MentionsRefresh").click(function () {
		kendo.ui.progress($("#MentionsTweets"), true);
		TwitterStream.mentionsSince = "";
		TwitterStream.tweetMentionsDataSource.transport.options.read.data.since = TwitterStream.mentionsSince;
		TwitterStream.tweetMentionsDataSource.transport.options.read.data.count = 0;
		$("#MentionsTweets .tweet").remove();
		TwitterStream.tweetMentionsDataSource.read();
	});
});



