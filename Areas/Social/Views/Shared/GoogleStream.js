﻿
var GoogleStream = {
	selectedAccountID: 0,
	postDataSource: null,
		
	ClearPost: function() {
		$(".google-post").remove();
	},
		
	LoadAccount: function (accountID) {
		$("#GoogleMore").hide();
		GoogleStream.selectedAccountID = accountID;
		GoogleStream.postDataSource.transport.options.read.data.accountID = accountID;
		GoogleStream.postDataSource.read();
	},

	dataBound: function (e) {
		kendo.ui.progress($("#Googleposts"), false);
		kendo.ui.progress($("#GoogleMoreDiv"), false);
			
		if (e.response != null && e.response.length > 0) {
			$("#GoogleMore").show();
				
			setTimeout(function () {
				$(".google-autogrow").autogrow();

				//$(".plusOne").click(function (e) {
				//	var me = $(this);
				//	var postId = me.data("id");
				//	var credId = me.data("credentialid");
				//	var post = me.closest(".google-post");
				//	kendo.ui.progress(post, true);
				//	var url = '/Social/Social/GooglePlusOne';
				//	$.post(url, { postID: postId, credentialID: credId }, function (data) {
				//		kendo.ui.progress(post, false);
				//		if (!data.success)
				//			SIGlobal.alertDialog("Post +1", data.message, "e");
				//		else {
				//			var count = $("#LikeCount" + postId).html();
				//			count = parseInt(count) + 1;
				//			$("#LikeCount" + postId).html(count);
				//		}
				//	});
				//});

				$(".google-post-delete").click(function () {
					var me = $(this);
					var postId = me.data("id");
					var credId = me.data("credentialid");
					var post = me.closest(".google-post");

					SIGlobal.confirmDialog("Delete Post", "Are you sure you want to Delete the Post?", function (button) {
						if (button == "Yes") {
							kendo.ui.progress(post, true);
							var url = '/Social/Social/GooglePostDelete';
							$.post(url, { postID: postId, credentialID: credId }, function (data) {
								kendo.ui.progress(post, false);
								if (!data.success)
									SIGlobal.alertDialog("Delete Post", data.message, "e");
								else {
									post.remove();
								}
							});
						}
					});
				});
				
				$(".comment-save").click(function() {
					var me = $(this);
					var postId = me.data("id");
					var credId = me.data("credentialid");
					var comment = $("#comment" + postId).val();

					kendo.ui.progress($("#commentArea" + postId), true);
					var url = '/Social/Social/GoogleCommentSave';
					$.post(url, { postID: postId, credentialID: credId, comment: comment }, function (data) {
						kendo.ui.progress($("#commentArea" + postId), false);
						if (data.success){
							var commentBlock = $("#postComments" + postId);
							commentBlock.data("kendoListView").dataSource.read();
						}
						else
							SIGlobal.alertDialog("Save Comment", data.message, "e");
					});
				})

				$(".google-post").each(function() {
					$(this).hover(function () { $(this).find(".google-post-delete").css("visibility", "visible"); }, function () { $(this).find(".google-post-delete").css("visibility", "hidden"); });

					var commentBlock = $(".post-comments", this);
					if (commentBlock.data("loaded") == false && commentBlock.data("count") != "0") {
						commentBlock.data("loaded", true);
						var id = commentBlock.data("id");
						var credId = commentBlock.data("credentialid");

						commentBlock.kendoListView({							
							dataSource: new kendo.data.DataSource({
								transport: {
									read: {
										url: '/Social/Social/GoogleStreamCommentsListView',
										dataType: "json",
										data: { gPostID: id, credentialID: credId, pageSize: 5 }
									}
								}
							}),
							dataBound: function() {
								$("#commentCount" + id).html(this.dataSource._data.length);

								if (this.dataSource._data.length > 0) {

									var comment = $(".comment", this.wrapper);
									comment.hover(function() { $(this).find(".delete").css("visibility", "visible"); }, function() { $(this).find(".delete").css("visibility", "hidden"); });
									comment.find(".delete").click(function() {
										var commentId = $(this).data("id");
										var postID = $(this).data("postid");
										var postComments = $("#postComments" + postID);
										var credId = postComments.data("credentialid");
										SIGlobal.confirmDialog("Delete Comment", "Are you sure you want to Delete the Comment?", function(button) {
											if (button == "Yes") {
												kendo.ui.progress(postComments, true);
												var url = '/Social/Social/GoogleCommentDelete';
												$.post(url, { commentID: commentId, credentialID: credId }, function (data) {
													kendo.ui.progress(postComments, false);
													if (!data.success)
														SIGlobal.alertDialog("Delete Comment", data.message, "e");
													else {
														postComments.data("kendoListView").dataSource.read();
													}
												});
											}
										});
									});
								}
							},
							template: kendo.template($("#GooglecommentTemplate").html())
						});
					}

					//return false; //for debugging just gets comments for first post
				});

			}, 100);
		}
	}
};

$(function () {
	var template = kendo.template($("#GooglestreamTemplate").html());
	GoogleStream.postDataSource = new kendo.data.DataSource({
		transport: {
			read: {
				url: '/Social/Social/GoogleStreamListView',
				dataType: "json",
				data: { accountID: GoogleStream.selectedAccountID, page: 1 }
			}
		},
		requestStart: function () {
			kendo.ui.progress($("#Googleposts"), true);
		},
		requestEnd: GoogleStream.dataBound,
		change: function () {
			$("#Googleposts").append(kendo.render(template, this.view()));
		}
	});

	if (treeSelectedAccount > 0) {
		GoogleStream.selectedAccountID = treeSelectedAccount;
		GoogleStream.postDataSource.transport.options.read.data.accountID = treeSelectedAccount;
		GoogleStream.postDataSource.transport.options.read.data.page = 1;
		GoogleStream.postDataSource.read();
	}
			
	$("#GoogleMore").hide();
	$("#GoogleMore").click(function () {
		kendo.ui.progress($("#GoogleMoreDiv"), true);
		GoogleStream.postDataSource.transport.options.read.data.page++;
		GoogleStream.postDataSource.read();
			
	});
})
	
