﻿
	
function getActions(status, statusDate, postID, postTargetID, canEdit) {
	//var canEdit = false;
	
	//if ((status == "Scheduled" && statusDate != "") || status == "WaitingOnApproval" || status == "SyndicationwithNoApproverHold")
	//	canEdit = true;
		
	var canRepost = false;
	if (status == "Failed" || status == "Published")
		canRepost = true;
		
	var now = new Date();
	var canDelete = false;
	if (status == "Scheduled" || status == "Failed" || status == "WaitingOnApproval" || status == "Published" || status == "SyndicationwithNoApproverHold")
		canDelete = true;
	if (status == "Processing" && now > new Date(statusDate).addMinutes(30).addYears(100))
		canDelete = true;

	var templt = kendo.template($("#command-template").html());
	return templt({ ID: postID, TargetID: postTargetID, canEdit: canEdit, canRepost: canRepost, canDelete: canDelete });
}
	
function getTypeImage(type, imageUrl) {
	var templt = kendo.template($("#type-template").html());
	return templt({ Type: type, PostImageURL: imageUrl });
}
	
function getStatus(status, failure, statusName) {

	failure = failure.replace("'", "");

	var templt = kendo.template($("#status-template").html());
	return templt({ Status: status, FailureMessage: failure, StatusName: statusName });
}
	
function statusFilter(e) {
	e.kendoDropDownList({
		dataSource: ["All", "Published", "Failed", "Scheduled"]
	});
}
	
function typeFilter(e) {
	e.kendoDropDownList({
		dataSource: ["All", "Photo", "Link", "Status"]
	});
}
	
function postedByFilter(e) {
	e.kendoDropDownList({
		dataTextField: "DisplayName",
		dataValueField: "UserID",
		dataSource: {
			transport: {
				read: {
					dataType: "json",
					url: '/Social/Social/PostActivityPostedBy',
					data: { locationID: filterViewModel.dealerFilterValue.Value, timeFrame: filterViewModel.timeFrameValue }
				}
			}
		}
	});
}
	
function filtersInit(e) {
	var filterMenu = e.sender.thead.find("th[data-field='Status']").data("kendoFilterMenu");
	if (filterMenu.form)
		filterMenu.form.find("span.k-dropdown:first").css("display", "none");
	
	filterMenu = e.sender.thead.find("th[data-field='Type']").data("kendoFilterMenu");
	if (filterMenu.form)
		filterMenu.form.find("span.k-dropdown:first").css("display", "none");
	
	filterMenu = e.sender.thead.find("th[data-field='PostedBy']").data("kendoFilterMenu");
	if (filterMenu.form)
		filterMenu.form.find("span.k-dropdown:first").css("display", "none");
}

function postDataBound(e) {
	$("#PostActivityList tbody tr .action-dropdown").each(function () {
		$(".menu", this).kendoMenu({
			direction: "left",
			openOnClick: true,
			select: function(e) {
				var action = $(e.item).data("action");
				if (!action) return;

				var menu = $(e.item).closest(".action-dropdown");
				var postId = menu.data("post-id");
				if (action == "edit") {
					ShowComposeWindow(postId, false);
				}
				if (action == "repost") {
					ShowComposeWindow(postId, true);
				} else {
					var url = '/Social/Social/PostAction?postTargetID=' + menu.data("target-id") + '&menuaction=' + action;
					if (action == "delete") {
						SIGlobal.confirmDialog("Delete", "Are you sure you want to Delete the Post?", function(button) {
							if (button == "Yes") {
								kendo.ui.progress($("#SocialPostActivity"), true);
								$.get(url, function(data) {
									kendo.ui.progress($("#SocialPostActivity"), false);
									if (data.success) {
										$('#PostActivityList').data("kendoGrid").dataSource.read();
									} else {
										SIGlobal.alertDialog("Delete Post Failed", "An Error has occured: " + data.message, "e");
									}
								});
							}
						});
					}
				}
			}
		});
	});
}
	
var viewModel = {
	networkFilterItems: [],
	networkFilterItemSource: new kendo.data.DataSource({
		transport: { read: { url: '/Admin/Admin/GetSocialNetworks', dataType: "json" } }
	}),
		
	oemFilterItems: [],
	oemFilterItemSource: new kendo.data.DataSource({
		transport: { read: { url: '/Reputation/Reputation/OEMList', dataType: "json" } }
	}),
		
	timeFrameValue: "60",
	timeFrames: [
		{ Value: "3", Text: "Last 3 days" },
		{ Value: "7", Text: "Last 7 days" },
		{ Value: "14", Text: "Last 14 days" },
		{ Value: "30", Text: "Last 30 days" },
		{ Value: "60", Text: "Last 60 days" },
		{ Value: "90", Text: "Last 90 days" },
		{ Value: "365", Text: "Last 365 days" },
	],
		
	groupFilterValue: null,
	groupFilterItemSource: new kendo.data.DataSource({
		transport: {
			read: { url: '/Reputation/Reputation/VirtualGroupList', dataType: "json" },
			parameterMap: function (data, type) {
				if (data.filter)
					return { text: data.filter.filters[0].value };
				return { text: "" };
			}
		},
		serverFiltering: true,
	}),
		
	dealerCurrentContext: 0,
	dealerFilterValue: null,
		
	dealerFilterItemSource: new kendo.data.DataSource({
		transport: {
			read: { url: '/Social/Social/DealerPostActivityAccounts', dataType: "json" },
			parameterMap: function (data, type) {
				var groupValue = "All";
				if (filterViewModel.groupFilterValue != null)
					groupValue = filterViewModel.groupFilterValue.Text;
				if (data.filter)
					return { virtualGroup: groupValue, dealerFilter: data.filter.filters[0].value };
				else if (filterViewModel.dealerCurrentContext > 0) {
					return { virtualGroup: groupValue, dealerFilter: "", dealerIDFilter: filterViewModel.dealerCurrentContext };
				}
				return { virtualGroup: groupValue, dealerFilter: "" };
			}
		},
		serverFiltering: true,
	}),
		
	//Events
	clearFilters: function (e) {
		e.preventDefault();
		this.groupFilterValue = null;
		this.dealerFilterValue = null;
		this.timeFrameValue = "60";
		this.oemFilterItems.length = 0;
		this.networkFilterItems.length = 0;
			
		kendo.bind($("#FilterView"), filterViewModel);
		this.timeFrameChange();
	},
	timeFrameChange: function (e) {
		this.applyNewFilters();
	},
	applyNewFilters: function(e) {
		var filterObj = [];
			
		filterObj.push({ field: "TimeFrameFilter", operator: "eq", value: this.timeFrameValue });
			
		if (this.networkFilterItems != null && this.networkFilterItems.length > 0) {
			if (this.networkFilterItems.length == 1) {
				filterObj.push({ field: "NetworkID", operator: "eq", value: this.networkFilterItems[0].ID });
			} else {
				var multiFilterObj = { logic: "or", filters: [] };
				for (var idx = 0; idx < this.networkFilterItems.length; idx++) {
					multiFilterObj.filters.push({ field: "NetworkID", operator: "eq", value: this.networkFilterItems[idx].ID });
				}
				filterObj.push(multiFilterObj);
			}
		}
		if (this.oemFilterItems != null && this.oemFilterItems.length > 0) {
			if (this.oemFilterItems.length == 1) {
				filterObj.push({ field: "OemID", operator: "eq", value: this.oemFilterItems[0].ID });
			} else {
				var multiFilterObj = { logic: "or", filters: [] };
				for (var idx = 0; idx < this.oemFilterItems.length; idx++) {
					multiFilterObj.filters.push({ field: "OemID", operator: "eq", value: this.oemFilterItems[idx].ID });
				}
				filterObj.push(multiFilterObj);
			}
		}
		if (this.dealerFilterValue != null) {
			filterObj.push({ field: "LocationID", operator: "eq", value: this.dealerFilterValue.Value });
		}
			
		if (this.groupFilterValue != null && this.groupFilterValue.Text != "All") {
			filterObj.push({ field: "VirtualGroupID", operator: "eq", value: this.groupFilterValue.Value });
		}
			
		$('#PostActivityList').data("kendoGrid").dataSource.filter(filterObj);
	}
};

function ShowComposeWindow(postID, rePost) {
	var kendoWindow = $("#PublishWindow").data("kendoWindow");
	if (!kendoWindow) {
		kendoWindow = $("#PublishWindow").kendoWindow({
			title: "Compose a Message",
			content: '/Social/Social/ComposeMessage?mode=False&postID=' + postID + '&repost=' + rePost,
			modal: true,
			iframe: false,
			draggable: true,
			visible: false,
			width: 1000,
			minHeight: 300,
			actions: ["Refresh", "Maximize", "Close"],
			refresh: function () {
				kendo.ui.progress($(".k-window"), false);
				$(".k-window-content").fadeIn("fast");
				this.center();
			}
		}).data("kendoWindow");
	} else {
		$(".k-window-content").hide();
		kendoWindow.refresh({ url: '/Social/Social/ComposeMessage?mode=False&postID=' + postID + '&repost=' + rePost });
	}

	kendo.ui.progress($(".k-window"), true);
	kendoWindow.center();
	kendoWindow.open();
}

$(function () {

	$("#SocialPostActivity").hide().css({ visibility: "visible" }).fadeIn("fast");

	$("#buttonComposeMessage").click(function() {
		ShowComposeWindow(0, false);
	});

	$(".post-grid").kendoTooltip({
		filter: ".view-full-text",
		width: 380,
		position: "bottom",
		showAfter: 600,
		autoHide: true,
		animation: {
			open: {
				effects: "fade:in",
				duration: 150
			}
		},
		content: { url: '/Social/Social/GetPostMessageToolTip' },
		requestStart: function(e) {
			var rowId = $(e.target).closest("tr").data("uid");
			var row = $("#PostActivityList").data("kendoGrid").dataSource.getByUid(rowId);
			e.options.url = '/Social/Social/GetPostMessageToolTip?id=' + row.PostTargetID;
		}
	});

	$(".post-grid").kendoTooltip({
		filter: ".post-image",
		width: 380,
		height: 310,
		position: "center",
		showAfter: 600,
		autoHide: true,
		animation: {
			open: {
				effects: "fade:in",
				duration: 150
			}
		},
		content: function(e) {
			var imageSrc = imageFolderPath + $(e.target).data("image");
			return "<img src='" + imageSrc + "' width='100%' />";
		}
	});

	$(".post-grid").kendoTooltip({
		filter: ".post-link",
		width: 380,
		height: 80,
		position: "bottom",
		showAfter: 600,
		autoHide: true,
		animation: {
			open: {
				effects: "fade:in",
				duration: 150
			}
		},
		content: { url: '/Social/Social/GetPostLinkToolTip' },
		requestStart: function(e) {
			var rowId = $(e.target).closest("tr").data("uid");
			var row = $("#PostActivityList").data("kendoGrid").dataSource.getByUid(rowId);
			e.options.url = '/Social/Social/GetPostLinkToolTip?id=' + row.PostTargetID;
		}
	});
});
	
