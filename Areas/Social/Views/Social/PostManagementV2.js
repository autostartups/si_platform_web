﻿

$(function () {
	$("#buttonComposeMessage").click(function () {
		ShowComposeWindow(0, false);
	});
});

function ShowComposeWindow(postID) {
	var kendoWindow = $("#PublishWindow").data("kendoWindow");
	if (!kendoWindow) {
		kendoWindow = $("#PublishWindow").kendoWindow({
			title: "Compose a Message",
			content: '/Social/Social/ComposeMessage?mode=False&postID=' + postID + '&repost=False&libraryPost=True',
			modal: true,
			iframe: false,
			draggable: true,
			visible: false,
			width: 1000,
			minHeight: 300,
			actions: ["Refresh", "Maximize", "Close"],
			refresh: function () {
				kendo.ui.progress($(".k-window"), false);
				$(".k-window-content").fadeIn("fast");
				this.center();
			}
		}).data("kendoWindow");
	} else {
		$(".k-window-content").hide();
		kendoWindow.refresh({ url: '/Social/Social/ComposeMessage?mode=False&postID=' + postID + '&repost=False&libraryPost=True' });
	}

	kendo.ui.progress($(".k-window"), true);
	kendoWindow.center();
	kendoWindow.open();
}

function ShowDetailsWindow(postId) {
	var kendoWindow = $("#PostDetailsWindow").data("kendoWindow");
	if (!kendoWindow) {
		kendoWindow = $("#PostDetailsWindow").kendoWindow({
			title: "Post Details",
			content: '/Social/Social/PostManagementDetails?postID=' + postId,
			modal: true,
			iframe: false,
			draggable: true,
			visible: false,
			width: 600,
			minHeight: 300,
			actions: ["Close"],
			refresh: function () {
				kendo.ui.progress($(".k-window"), false);
				$(".k-window-content").fadeIn("fast");
				this.center();
			}
		}).data("kendoWindow");
	} else {
		$(".k-window-content").hide();
		kendoWindow.refresh({ url: '/Social/Social/PostManagementDetails?postID=' + postId });
	}

	kendo.ui.progress($(".k-window"), true);
	kendoWindow.center();
	kendoWindow.open();
}


function PostSaved(message) {
	$("#PublishWindow").data("kendoWindow").close();
	$("#saveResults").html(message);
}

function postDataBound(e) {
	$("#PostActivityList tbody tr .action-dropdown").each(function () {
		$(".menu", this).kendoMenu({
			direction: "left",
			openOnClick: true,
			select: function (e) {
				var action = $(e.item).data("action");
				if (!action) return;

				var menu = $(e.item).closest(".action-dropdown");
				var postId = menu.data("post-id");
				if (action == "edit") {
					ShowComposeWindow(postId, false);
				}
				else if (action == "history") {
					document.location.href = "/Social/PostActivity";
				}
				else if (action == "details") {
					ShowDetailsWindow(postId);
				}
				else {
					var url = '/Social/Social/PostManagementV2Action?postID=' + postId + '&menuaction=' + action;
					if (action == "delete") {
						SIGlobal.confirmDialog("Delete", "Are you sure you want to Delete all these Post?", function (button) {
							if (button == "Yes") {
								kendo.ui.progress($("#SocialPostActivity"), true);
								$.get(url, function (data) {
									kendo.ui.progress($("#SocialPostActivity"), false);
									if (data.success) {
										$('#PostActivityList').data("kendoGrid").dataSource.read();
									} else {
										SIGlobal.alertDialog("Delete Post Failed", "An Error has occured: " + data.message, "e");
									}
								});
							}
						});
					}
				}
			}
		});
	});
}

function TimeFrameChange() {
	$("#PostActivityList").data("kendoGrid").dataSource.read();
}

function getActions(postID, canEdit, canDelete) {
	var templt = kendo.template($("#command-template").html());
	return templt({ ID: postID, canEdit: canEdit, canDelete: canDelete });
}

function getTypeImage(type) {
	var templt = kendo.template($("#type-template").html());
	return templt({ Type: type });
}

function getTimeFrame() {
	var timeFrameDD = $("#TimeFrame").data("kendoDropDownList");
	timeFrame = 60;
	if (timeFrameDD != null) {
		timeFrame = timeFrameDD.value();
	}

	return {
		timeFrame: timeFrame
	}
}
