﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SI.BLL;
using SI.DTO;
using SI.Services.Dashboard;
using SI.Services.Dashboard.Models;
using SI.Services.Public;
using SI.Services.Social;
using SI.WEB.Portal.Framework;
using SI.WEB.Portal.Models;

namespace SI.WEB.Portal.Areas.Mobile.Controllers
{
	[Authorize]
	[RouteArea("Mobile")]
    public class MobileController : Controller
    {
		[Route("PostApproval", RouteName = "MobilePostApproval")]
        public ActionResult PostApproval()
		{
			ViewBag.PostTargetId = 0;
			if (Session["MobileApproval_PostTargetID"] != null)
				ViewBag.PostTargetId = (long)Session["MobileApproval_PostTargetID"];
			
            return View();
        }

		public JsonResult ApprovalsList([DataSourceRequest] DataSourceRequest request)
		{
			BaseMembersModel model = new BaseMembersModel();

			List<NotificationsApprovals> approvals = DashboardService.GetPendingApprovals(model.UserInfo);

			return Json(approvals, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public ActionResult PostSave(long? postTargetID, long? postID, long? accountID, string message, DateTime scheduleDate)
		{
			BaseMembersModel model = new BaseMembersModel();

			string modelErrors = "";
			List<string> Problems = new List<string>();
			bool success = SocialService.SaveMobileApproval(model.UserInfo, postTargetID.GetValueOrDefault(), postID.GetValueOrDefault(), accountID.GetValueOrDefault(), message, scheduleDate, ref Problems);

			if (!success)
				modelErrors = string.Format("Unable to Update Post at this time. {0}", SIHelpers.EntityErrorsToHtml(Problems));

			return Json(new {success, message = modelErrors});
		}
    }
}
