﻿
var lastSelectedId = 0;
var EditSaved = false;
var keyupTimer = null;
	
function onEditClose() {
	if (EditSaved) {
		$("#AccountList").data("kendoGrid").dataSource.read();
		//return;
	}
	$("#AccountEdit form").remove(); //clean up DOM because of reusing window
}

function AccountsChange(e) {
	var data = this.dataItem(this.select());
	lastSelectedId = data.ID;
}
	
function AccountSelect1x(e) {
	var data = this.dataItem(this.select());
	lastSelectedId = data.ID;
	AccountsPop();
}
	
function SwitchContext(e) {
	var data = this.dataItem(this.select());
	var id = data.ID;
		
	$.post('/members/setaccount', { id: id }, function () {
		window.location.reload();
	});

}

function AccountsPop() {
	$("#LocationEdit").hide();
	$("#LoadingLoacations").show();
	$("#AccountEdit_wnd_title").html('');

	var kendoWindow = $("#AccountEdit").kendoWindow({
		title: "",
		content: '/Admin/Admin/AccountEdit' + '?accountID=' + lastSelectedId,
		modal: true,
		iframe: false,
		draggable: true,
		resizable: true,
		visible: false,
		width: 880,
		minWidth: 880,
		height: 520,
		minHeight: 520,
		actions: ["Maximize", "Close"],
		close: onEditClose

	}).data("kendoWindow");

	kendoWindow.center();
	kendoWindow.open();
}

var filterViewModel = kendo.observable({
	accountNameFilterValue: "",

	packageFilterItems: [],
	packageFilterItemSource: new kendo.data.DataSource({
		transport: { read: { url: '/Admin/Admin/GetPackagesList', dataType: "json" } }
	}),

	oemFilterItems: [],
	oemFilterItemSource: new kendo.data.DataSource({
		transport: { read: { url: '/Reputation/Reputation/OEMList', dataType: "json" } }
	}),
		
	//Events
	clearFilters: function(e) {
		e.preventDefault();
		this.oemFilterItems.length = 0;
		this.packageFilterItems.length = 0;
		this.accountNameFilterValue = "";
		kendo.bind($("#FilterView"), filterViewModel);

		var ds = $("#AccountList").data().kendoGrid.dataSource;
		ds.transport.options.read.url = '/Admin/Admin/AccountListGrid';
		this.applyNewFilters();
	},
	addAccount: function(e) {
		lastSelectedId = 0;
		AccountsPop();
	},
	applyNewFilters: function(e) {
		var filterObj = [];

		if (this.oemFilterItems != null && this.oemFilterItems.length > 0) {
			if (this.oemFilterItems.length == 1) {
				filterObj.push({ field: "OemID", operator: "eq", value: this.oemFilterItems[0].ID });
			} else {
				var multiFilterObj = { logic: "or", filters: [] };
				for (var idx = 0; idx < this.oemFilterItems.length; idx++) {
					multiFilterObj.filters.push({ field: "OemID", operator: "eq", value: this.oemFilterItems[idx].ID });
				}
				filterObj.push(multiFilterObj);
			}
		}

		if (this.packageFilterItems != null && this.packageFilterItems.length > 0) {
			if (this.packageFilterItems.length == 1) {
				filterObj.push({ field: "PackageID", operator: "eq", value: this.packageFilterItems[0].PackageID });
			} else {
				var multiFilterObj = { logic: "or", filters: [] };
				for (var idx = 0; idx < this.packageFilterItems.length; idx++) {
					multiFilterObj.filters.push({ field: "PackageID", operator: "eq", value: this.packageFilterItems[idx].PackageID });
				}
				filterObj.push(multiFilterObj);
			}
		}

		if (this.accountNameFilterValue != null && this.accountNameFilterValue != "") {
			filterObj.push({ field: "Name", operator: "startswith", value: this.accountNameFilterValue });
		}

		$('#AccountList').data("kendoGrid").dataSource.filter(filterObj);
		$('#AccountList').data("kendoGrid").refresh();
	},

	searchSelect: function(e) {
		this.accountNameFilterValue = e.target.value;

		if (keyupTimer != null)
			clearTimeout(keyupTimer);

		keyupTimer = setTimeout(function() {
			filterViewModel.applyNewFilters();
		}, 600);
	},
});

$(function() {
	$("#AdminAccounts").hide().css({ visibility: "visible" }).fadeIn("fast");

	$("#AccountList").on("dblclick", "tr:not(:has(th))", function() {
		AccountsPop();
	});
});
	




