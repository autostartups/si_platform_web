﻿
function getActions(userID, canPossess) {
	var templt = kendo.template($("#action-template").html());
	return templt({ ID: userID, canPossess: canPossess });
}

function UserMenuAction(action, id, menuItem) {
	$("#LoadingIconSave" + id).show();
	var url = '/Admin/Admin/UserActionMenu?userID=' + id + '&menuaction=' + action;
	$.get(url, function (data) {
		$("#LoadingIconSave" + id).hide();
		SIGlobal.alertDialog("User Action", data.message);
		$("#UsersList").data("kendoGrid").dataSource.read();
	});
}

function hideProgress() {
	kendo.ui.progress($(".admin-users"), false);
}

function Impersonate(userID){
	var url = '/Account/Impersonate/' + userID;
	kendo.ui.progress($("#AdminUsers"), true);
	setTimeout(function () {
		$.get(url, function (data) {
			if (data) {
				window.location.reload(true);
			} else {
				kendo.ui.progress($("#AdminUsers"), false);
			}
		});
	}, 200);
}


function usersDataBound() {

	$("#UsersList tbody tr .action-dropdown").each(function () {
		$(".menu", this).kendoMenu({
			direction: "left",
			openOnClick: true,
			select: function (e) {
				var action = $(e.item).data("action");
				if (!action) return;

				var userId = $(e.item).data("userid");
				if (action == "possess")
					Impersonate(userId);
				else
					UserMenuAction(action, userId, e.item);
			}
		});
	});
}

var userID;
var LastDetailsRow;
var lastDetailDiv;

function UserDetails(arg) {
	if (LastDetailsRow != null && LastDetailsRow.index() != arg.masterRow.index()) {
		arg.sender.collapseRow(LastDetailsRow);
		$(lastDetailDiv).empty();
	}

	var dataItem = arg.sender.dataSource.getByUid(arg.masterRow.data("uid"));
	var detailsDiv = arg.detailRow.find(".user-details");
	if (detailsDiv) {
		LastDetailsRow = arg.masterRow;
		lastDetailDiv = detailsDiv;
		var top = arg.detailRow.offset().top - $(".k-grid-content").offset().top - arg.masterRow.height();

		detailsDiv.load("/Admin/Admin/UserDetailEdit?userID=" + dataItem.UserID + '&accountName=' + encodeURIComponent(dataItem.AccountName) + '&accountID=' + dataItem.AccountID, function () {
			$(".k-grid-content").animate({ scrollTop: '+=' + top + 'px' }, 200);
		});
	}
}

var timerID = null;
var filterViewModel = kendo.observable({

	userNameFilterValue: "",
		
	accountFilterValue: null,
	accountFilterItemSource: new kendo.data.DataSource({
		transport: {
			read: { url: "/Admin/Admin/AccountListOfUsersMembers", dataType: "json" },
			parameterMap: function (data, type) {
				if (data.filter)
					return { text: data.filter.filters[0].value };
				return { text: "" };
			}
		},
		serverFiltering: true,
	}),
		
	//Events
	clearFilters: function (e) {
		e.preventDefault();
		this.accountFilterValue = null;
		this.userNameFilterValue = "";
		kendo.bind($("#FilterView"), filterViewModel);
		$('#UsersList').data("kendoGrid").dataSource.filter(null);
	},
	applyNewFilters: function(e) {
		var filterObj = [];
			
		if (this.accountFilterValue != null && this.accountFilterValue.Text != "") {
			filterObj.push({ field: "AccountID", operator: "eq", value: this.accountFilterValue.ID });
		}

		if (this.userNameFilterValue != null && this.userNameFilterValue != "") {
			filterObj.push({ field: "LastName", operator: "eq", value: this.userNameFilterValue });
		}

		if (filterObj.length > 0) {
			kendo.ui.progress($(".admin-users"), true);
			$('#UsersList').data("kendoGrid").dataSource.filter(filterObj);
			$('#UsersList').data("kendoGrid").refresh();
		}
	},

	searchSelect: function (e) {
		this.userNameFilterValue = e.target.value;

		if (timerID != null)
			clearTimeout(timerID);
		timerID = setTimeout(function (){
			filterViewModel.applyNewFilters();
		}, 600);
	},
});
	
$(function () {
	kendo.bind($("#FilterView"), filterViewModel);
	$("#AdminUsers").hide().css({ visibility: "visible" }).fadeIn("fast");

		
});	
	

