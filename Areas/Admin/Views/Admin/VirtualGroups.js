﻿


var SCVirtualGroup = {
	kendoValidator: null,
	justSaved: false,
	lastSavedID: 0,

	virtualGroupBound: function(e) {
		setTimeout(function() {
			var grid = $("#VirtualGroupGrid").data("kendoGrid");

			var row;
			if (SCVirtualGroup.lastSavedID == 0) {
				row = e.sender.tbody.find('tr:first');
			} else {
				var data = grid.dataSource.data();
				for (var idx = 0; idx < data.length; idx++) {
					var item = data[idx];
					if (item.ID == SCVirtualGroup.lastSavedID) {
						row = e.sender.tbody.find("tr[data-uid='" + item.uid + "']");
						break;
					}
				}
			}
			if (row) {
				grid.select(row);
				row.trigger('click');
			}
		}, 100);
	},

	VirtualGroupInfoSave: function(response) {
		$("#resultsVG").html(response.message);
		$("#VirtualGroupGrid").data("kendoGrid").dataSource.read();
		SCVirtualGroup.submitCountVG = 0;
		SCVirtualGroup.justSaved = true;
	},

	submitCountVG: 0,
	BeginSubmitVirtualGroup: function() {
		if (SCVirtualGroup.submitCountVG > 0) return false;
		SCVirtualGroup.submitCountVG++;

		EditSaved = true;
	},

	virtualGroupEdit: function(e) {
		var data = this.dataItem(this.select());
		SCVirtualGroup.lastSavedID = data.ID;

		$("#EditTitle").html(kendo.format("Edit Virtual Group - {0}", data.Name));
		$("#Name").val(data.Name);
		$("#ID").val(data.ID);
		//reset accessible
		$('#listBoxTo option').appendTo('#listBoxFrom');
		$('#listBoxFrom option').removeAttr('selected');

		if (!SCVirtualGroup.justSaved) {
			$("#resultsVG").html("");
			SIGlobal.ValidationSummaryClear();
		}
		SCVirtualGroup.justSaved = false;

		$("#SelectedLocationIDs").val("");
		if (data.SelectedLocationIDs.length > 0) {
			for (var idx = 0; idx < data.SelectedLocationIDs.length; idx++) {
				$("#listBoxFrom option[value=" + data.SelectedLocationIDs[idx] + "]").attr("selected", "selected");
			}
			$("#SelectedLocationIDs").val(data.SelectedLocationIDs.join(","));
			$("#moveToTo").trigger("click");
		}


		$("#Name").removeClass("input-validation-error").removeClass("dirtyFlag");
	}
};
	

$(function () {
	SCVirtualGroup.kendoValidator = $("#formVirtualGroups").kendoValidator().data("kendoValidator");
	$("#formVirtualGroups").dirtyFlagSet();
	$(".ico-required").attr("title", "Required");
	SIGlobal.placeHolderSim(this);

	$.dualListBoxes();

	$("#SearchVGText").keyup(function (e) {
		var search = $(this).val();
		var filterObj = [];
		if (search != null && search != "") {
			filterObj.push({ field: "Name", operator: "startswith", value: search });
		}

		$('#VirtualGroupGrid').data("kendoGrid").dataSource.filter(filterObj);
	});

	$("#NewVGButton").on("click", function () {
		$("#Name").val("");
		$("#ID").val("0"); //THIS INDICATES A NEW VIRTUAL GROUP
		$("#SelectedLocationIDs").val("");
		$("#moveAllToFrom").trigger("click");
		$("#EditTitle").html("* New Virtual Group *");
		$("#VirtualGroupGrid tbody tr").removeClass("k-state-selected");
		$("#resultsVG").html("");
		$("#Name").focus();
	});

	$("#saveVGButton").click(function() {

		if (SCVirtualGroup.kendoValidator.validate()) {
			SIGlobal.ValidationSummaryClear();
			$("#resultsVG").html("");

			var selectedIDs = [];
			$("#SelectedLocationIDs").val("");
			$("#listBoxTo option").each(function() {
				selectedIDs.push($(this).val());
			});
			if (selectedIDs.length > 0)
				$("#SelectedLocationIDs").val(selectedIDs.join(","));

			$("#formVirtualGroups").submit();
		} else {
			SIGlobal.ValidationSummaryPopulate(SCVirtualGroup.kendoValidator.errors());
		}
	});
});
