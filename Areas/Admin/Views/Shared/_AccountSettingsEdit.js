﻿

var AccountSettings = {
	kendoValidator: null,
	submitCountAccountSettings: 0,
		
	BeginSubmit: function() {
		if (AccountSettings.submitCountAccountSettings > 0) return false;
		AccountSettings.submitCountAccountSettings++;

		$(".validation-summary-errors").removeClass("validation-summary-errors").addClass("validation-summary-valid");
		EditSaved = true;
	},
		
	SaveFailure: function () {
		SIGlobal.formSaveFailure();
		AccountSettings.submitCountAccountSettings = 0;
	},

	InfoSave: function(response) {
		$("#formAccountSettings").dirtyFlagReset();
		$("#resultsAccountSettings").html(response.message);
		AccountSettings.submitCountAccountSettings = 0;
	}
};

$(function() {
	AccountSettings.kendoValidator = $("#formAccountSettings").kendoValidator().data("kendoValidator");
		
	$("#saveButtonAccountSettings").click(function () {
		if (AccountSettings.kendoValidator.validate()) {
			SIGlobal.ValidationSummaryClear();
			$("#resultsAccountSettings").html("");

			$("#formAccountSettings").submit();
		} else {
			SIGlobal.ValidationSummaryPopulate(AccountSettings.kendoValidator.errors());
		}
	});
});


