﻿
var SocialConfig = {
	AccountID: '',
	
	listViewBound: function () {
		$(".action-refresh").click(function () {
			var button = $(this);
				
			var configID = button.data("id");
			var url = '/Admin/Admin/RefreshSocialConnection';
			var addData = { configID: configID, accountID: SocialConfig.AccountID };
			$.post(url, addData, function (data) {
				if (data.Success)
					$("#SocialConfigListView").data("kendoListView").dataSource.read();
				else
					SocialConfig.OpenAuthWindow(button);
			});
		});

		$(".action-button").click(function () {
			var button = $(this);
			SocialConfig.OpenAuthWindow(button);
		});

		$(".action-disconnect").click(function () {
			var id = $(this).data("id");

			var url = '/Admin/Admin/DisconnectSocialConnection';
			var addData = { configID: id, accountID: SocialConfig.AccountID };
			$.post(url, addData, function (data) {
				if (data.Success)
					$("#SocialConfigListView").data("kendoListView").dataSource.read();
			});
		});
	},
		
	OpenAuthWindow: function (button) {
		var thisButton = button;
		var loginUrl = button.data("url");
		var socialappid = button.data("socialappid");
		var context = button.data("context");
		var network = button.data("network");
		var configId = button.data("id");

		SocialConfig.socialLoginWindow = window.open(loginUrl, "Login", "width=600,height=500,toolbar=no,status=no,resizable=yes,scrollbars=yes,modal=no");
		SocialConfig.socialPoleCount = 0;

		var connectionPage = "#connectionPages_" + socialappid;
		$(connectionPage).html("");

		SocialConfig.socialAuthTimer = setInterval(function () {

			if ($(connectionPage).html() == "" && SocialConfig.socialPoleCount < 90) {
				SocialConfig.socialPoleCount++;
				$.get("/public/GetSocialAuthCode?stateContext=" + context, function (data) {
					if (data.code != "") {
						clearInterval(SocialConfig.socialAuthTimer);
						clearInterval(SocialConfig.loginWindowCheck);
						SocialConfig.SocialAppID = socialappid;
						SocialConfig.ConfigID = configId;
							
						if (network == "facebook") {
							$(connectionPage).load('/Admin/Admin/FacebookAccounts' + '?code=' + data.code + '&accountID=' + SocialConfig.AccountID, function () {
								thisButton.hide();
							});
						} else if (network == "google") {
							$(connectionPage).load('/Admin/Admin/GooglePages' + '?code=' + data.code + '&accountID=' + SocialConfig.AccountID, function () {
								thisButton.hide();
							});
						//} else if (network == "youtube") {
						//	$(connectionPage).load('/Admin/Admin/YouTubeChannels' + '?code=' + data.code + '&accountID=' + SocialConfig.AccountID, function() {
						//		thisButton.hide();
						//	});
						} else {
							var url = '/Admin/Admin/SaveSocialConnection';
							var addData = { configID: configId, accountID: SocialConfig.AccountID, authCode: data.code, network: network, socialappid: socialappid };
							$.post(url, addData, function(data) {
								if (data.Success)
									$("#SocialConfigListView").data("kendoListView").dataSource.read();
							});
						}
					}
				});
			} else {
				clearInterval(SocialConfig.socialAuthTimer);
				clearInterval(SocialConfig.loginWindowCheck);
				if (SocialConfig.socialLoginWindow != null)
					SocialConfig.socialLoginWindow.close();

				$(connectionPage).html("");
			}
		}, 1000);

		SocialConfig.loginWindowCheck = setInterval(function() {
			if (!SocialConfig.socialLoginWindow || SocialConfig.socialLoginWindow.closed) {
				clearInterval(SocialConfig.socialAuthTimer);
				clearInterval(SocialConfig.loginWindowCheck);
			}
		}, 2000);
	},
		
	socialAuthTimer: '',
	socialPoleCount: 0,
	socialLoginWindow: null,
	loginWindowCheck: 0,
	facebookAccountID: 0,
	SocialAppID: 0,
	ConfigID: 0,
		
	LoadAccountFromFB: function (pageID, website, network) { 
		$(".connectionPages").hide();
		var url = '/Admin/Admin/SaveSocialPageConnectionConfig';
		var addData = { network: network, pageID: pageID, website: website, configId: SocialConfig.ConfigID, accountID: SocialConfig.AccountID, socialAppId: SocialConfig.SocialAppID };
		$.post(url, addData, function (data) {
			if (data.Success) {
				$("#SocialConfigListView").data("kendoListView").dataSource.read();
			} else
				$("#error_" + SocialConfig.SocialAppID).html("Could not save the Page Connection.");
		});
	},
		
	ShowLoading: function() {
		kendo.ui.progress($(".config-section"), true);
	},
	HideLoading: function() {
		kendo.ui.progress($(".config-section"), false);
	},
	
	Init: function() {
		SIGlobal.facebookAccountSelectCallback = SocialConfig.LoadAccountFromFB;
	}
};



