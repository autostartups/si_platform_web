﻿

var RepConfig = {
	AccountID: '',
	
	submitCount: 0,
	BeginSubmitReputation: function() {
		if (RepConfig.submitCount > 0) return false;
		RepConfig.submitCount++;
		
		EditSaved = true;
	},

	SaveFailure: function () {
		SIGlobal.formSaveFailure();
		RepConfig.submitCount = 0;
	},
		
	ReputationInfoSave: function(response) {
		$("#formReputationConfig").dirtyFlagReset();
		$("#resultsRCTS").html(response.message);
		RepConfig.submitCount = 0;

		if (response.success) {
			kendo.ui.progress($("#ReputationConfigTab"), true);
			var tabs = $("#ConfigTabs").data("kendoTabStrip");
			tabs.reload(tabs.items()[1]);
		}
	},
	
	Init: function() {
		$("#formReputationConfig").dirtyFlagSet();

		$("#AutoPopulate").click(function () {
			kendo.ui.progress($(".config-section"), true);
			var changed = false;

			$(".linkItemAutoPop").each(function () {
				var me = $(this);
				var url = '/Admin/Admin/ReputationConfigAutoPopulate';
				var addData = { accountID: RepConfig.AccountID, source: me.data("source") };
				$.ajax({ type: "POST", url: url, data: addData, async: false, dataType: "json" }).done(function(data) {
					if (data.Success) {
						$("#" + me.data("input")).val(data.url).addClass("dirtyFlag");
						changed = true;
					}
				});
			});

			kendo.ui.progress($(".config-section"), false);
			if (changed)
				$("#resultsRCTS").html("Configuration has changed, remember to Save.")
		});

		$(".linkItemAutoPop").click(function () {
			kendo.ui.progress($(".config-section"), true);
			var me = $(this);
			var url = '/Admin/Admin/ReputationConfigAutoPopulate';
			var addData = { accountID: RepConfig.AccountID, source: me.data("source") };
			$.post(url, addData, function (data) {
				kendo.ui.progress($(".config-section"), false);
				if (data.Success) {
					$("#" + me.data("input")).val(data.url).addClass("dirtyFlag");
					$("#resultsRCTS").html("Configuration has changed, remember to Save.")
				} else
					SIGlobal.alertDialog("Auto Populate", "No configuration found.", "i");
			});
		});

		$(".link-field").on('propertychange keyup input paste', function () {
			var io = $(this).val().length ? 1 : 0;
			$(this).next('.iconclear').stop().fadeTo(300, io);
		});
		$(".iconclear").on('click', function () {
			$(this).delay(300).fadeTo(300, 0).prev('input').val('').addClass("dirtyFlag");
		});
		$(".link-field").each(function () {
			if ($(this).val().length)
				$(this).next('.iconclear').fadeTo(300, 1);
		})
		$(".linkItemBrowser").on("click", function () {
			var input = $(this).data("input");
			var url = $("#" + input).val();
			window.open(url);
		})
	}
};
	

