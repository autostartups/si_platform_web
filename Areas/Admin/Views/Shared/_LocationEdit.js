﻿
	
var LocationEdit = {
	
	prefix: "",
	FacebookLoginURL: '',
	FacebookStateContext: '',
	resellerAccountID: '',
	formSelector: "#formLocationEdit", 

	submitCount: 0,
	BeginSubmit: function() {
		if (LocationEdit.submitCount > 0) return false;
		LocationEdit.submitCount++;
		EditSaved = true;
	},

	SaveFailure: function () {
		SIGlobal.formSaveFailure();
		LocationEdit.submitCount = 0;
	},
		
	loadAccountID: '',
	loadAccountName: '',
	loadNewSubAccount: 'false',
	LocationInfoSave: function(response) {
		$(LocationEdit.formSelector).dirtyFlagReset();
		$("#resultsTS" + LocationEdit.prefix).html(response.message);
		LocationEdit.submitCount = 0;
			
		if (response.success && LocationEdit.loadAccountID == '0') {
			if (LocationEdit.loadNewSubAccount == 'false') {
				var newAccountID = response.accountID;
				$("#AccountEdit").data("kendoWindow").refresh({ url: '/Admin/Admin/AccountEdit?accountID=' + newAccountID });
			} else {
				SubAccounts.NewAccountCreated($("#" + LocationEdit.prefix + "_Name").val());
			}
		}
	},
		
	facebookAuthTimer: '',
	facebookPoleCount: 0,
	facebookLoginWindow: null,
	facebookAccountID: 0,
		
	LoadAccountFromFB: function(facebookID) {
		LocationEdit.facebookAccountID = facebookID;

		var preFix = LocationEdit.prefix;
		if (preFix != '') preFix += '_';
			
		$("#"+ preFix + "FacebookAccountID").val(facebookID);

		$.get('/Admin/Admin/GetFacebookAccountDetails?facebookAccountID=' + facebookID, function(data) {
			if (data.Success == true) {
				$("#" + preFix + "Name").val(data.Name);
				$("#" + preFix + "DisplayName").val(data.DisplayName);
				$("#" + preFix + "StreetAddress1").val(data.StreetAddress1);
				$("#" + preFix + "WebsiteURL").val(data.WebsiteURL);
				$("#" + preFix + "CityName").val(data.CityName);
				$("#" + preFix + "PhoneNumber").val(data.PhoneNumber);
				$("#" + preFix + "PostalCode").val(data.PostalCode);

				$("#" + preFix + "CountryID").data("kendoDropDownList").select(function(ditem) {
					return ditem.Name == data.CountryName;
				});
			}
		});

		$("#FormSection" + LocationEdit.prefix).fadeIn("fast");
		$("#FacebookFrame" + LocationEdit.prefix).fadeOut("fast");
		$("#accountPicker" + LocationEdit.prefix).html("");
	},
		
	filterStates: function() {
		var preFix = LocationEdit.prefix;
		if (preFix != '') preFix += '_';
		return { countryID: $("#" + preFix + "CountryID").val() };
	},

	AccountTypeChange: function(e) {
		if ($("#AccountTypeID").data("kendoDropDownList").text() == "grouping") {
			$(".required-company").hide();
		} else {
			$(".required-company").show();
		}
	},
		
	kendoValidator: null,
	
	Init: function() {
		LocationEdit.formSelector = LocationEdit.formSelector + LocationEdit.prefix;
		
		LocationEdit.kendoValidator = $(LocationEdit.formSelector).kendoValidator().data("kendoValidator");

		$(LocationEdit.formSelector).dirtyFlagSet();
		var postAdr = $(LocationEdit.formSelector).attr("action");
		$(LocationEdit.formSelector).attr("action", postAdr + LocationEdit.prefix);
		$(".ico-required").attr("title", "Required");

		$("#saveButton" + LocationEdit.prefix).click(function () {
			if (LocationEdit.kendoValidator.validate()) {
				SIGlobal.ValidationSummaryClear();
				$("#resultsTS" + LocationEdit.prefix).html("");

				$(LocationEdit.formSelector).submit();
			} else {
				SIGlobal.ValidationSummaryPopulate(LocationEdit.kendoValidator.errors());
			}
		});

		if (LocationEdit.loadNewSubAccount == 'false') {
			var title = "";
			if (LocationEdit.loadAccountID != '0')
				title = 'Account Edit - ' + LocationEdit.loadAccountName;
			else
				title = '* New Account *';
			$("#AccountEdit").data("kendoWindow").title(title.replace(/&amp;/g, '&'));
		} else {
			$("#NewAccountCancel" + LocationEdit.prefix).show().click(SubAccounts.NewAccountCancel);
		}

		if (LocationEdit.loadAccountID == '0') {
			$("#FacebookLoginButton" + LocationEdit.prefix).click(function () {
				//check if IE and open new window
				if ($("html").hasClass("k-ie")) {
					LocationEdit.facebookLoginWindow = window.open(LocationEdit.FacebookLoginURL, "Facebook Login", "width=600,height=300,toolbar=no,status=no,resizable=no,scrollbars=no,modal=yes");
				} else {
					$("#FacebookFrame" + LocationEdit.prefix).attr("src", LocationEdit.FacebookLoginURL);
				}
				$("#FormSection" + LocationEdit.prefix).fadeOut("fast");
				$("#FacebookFrame" + LocationEdit.prefix).fadeIn("fast");
				LocationEdit.facebookPoleCount = 0;

				LocationEdit.facebookAuthTimer = setInterval(function () {
					if ($("#accountPicker" + LocationEdit.prefix).html() == "" && LocationEdit.facebookPoleCount < 90) {
						LocationEdit.facebookPoleCount++;
						$.get("/public/GetSocialAuthCode?stateContext=" + LocationEdit.FacebookStateContext, function (data) {
							if (data.code != "") {
								clearInterval(LocationEdit.facebookAuthTimer);
								$("#accountPicker" + LocationEdit.prefix).load('/Admin/Admin/FacebookAccounts?code=' + data.code + '&accountID=' + LocationEdit.resellerAccountID, function () {
									$("#FacebookFrame" + LocationEdit.prefix).hide();
								});
								$("#FacebookLoginButton" + LocationEdit.prefix).hide();
							}
						});
					} else {
						clearInterval(LocationEdit.facebookAuthTimer);
						if (LocationEdit.facebookLoginWindow != null)
							LocationEdit.facebookLoginWindow.close();
						$("#FormSection" + LocationEdit.prefix).fadeIn("fast");
						$("#FacebookFrame" + LocationEdit.prefix).fadeOut("fast");
						$("#accountPicker" + LocationEdit.prefix).html("");
					}
				}, 1000);
			});
		}

		SIGlobal.facebookAccountSelectCallback = LocationEdit.LoadAccountFromFB;
	}
};

