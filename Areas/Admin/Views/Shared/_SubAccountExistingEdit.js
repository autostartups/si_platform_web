﻿
var SubAccountExisting = {
	AccountID: '',
	
	ApplyAccountExistingFilters: function() {
		var filterObj = [];
		var search = $("#SearchTextAccountExist").val();
		if (search != null && search != "" && search.length > 1) {
			filterObj.push({ field: "Name", operator: "startswith", value: search });
			$('#AccountExistGrid').data("kendoGrid").dataSource.filter(filterObj);
		}
	},
	
	AddExistingAccount: function (e) {
		$("#LoadingIconSaveSAEE").show();
		var data = this.dataItem(this.select());
		var url = '/Admin/Admin/SubAccountAddAccount';
		var addData = { subAccountID: data.ID, accountID: SubAccountExisting.AccountID };
		$.post(url, addData, function (data) {
			$("#LoadingIconSaveSAEE").hide();
			if (data.Success) {
				$("#SubAccountGrid").data("kendoGrid").dataSource.read();
				$("#AccountExistGrid").data("kendoGrid").dataSource.read();
				$("#AccountExistGrid").data("kendoGrid").refresh();
			} else
				SIGlobal.alertDialog("Add Existing Account", data.message, "e");
		});

	},
	
	Init: function() {
		$("#SearchTextAccountExist").keyup(function (e) {
			SubAccountExisting.ApplyAccountExistingFilters();
		});

	}
};
