﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SI.BLL;
using SI.DTO;
using SI.Extensions;
using SI.Services.Reports;
using SI.Services.Reports.Models;
using SI.WEB.Portal.Framework;
using SI.WEB.Portal.Models;
using Telerik.Reporting;

namespace SI.WEB.Portal.Areas.Reports.Controllers
{
    [Authorize]
    [RouteArea("Reports")]
    public class ReportsController : Controller
    {
        [Route("SAMReport", RouteName = "ReportsSAMReport")]
        [BaseMembersModelAction]
        public ActionResult SAMReport()
        {
            return View();

        }


        public JsonResult SAMReportGrid([DataSourceRequest] DataSourceRequest request, string timeFrame)
        {
            //TODO DUMMY DATA

            BaseMembersModel model = new BaseMembersModel();
            List<SAMReport> samReports = ReportsService.SAMReportGrid(model.UserInfo, KendoSupport.PagedGridRequest(request), timeFrame);

            return Json(samReports.ToDataSourceResult(request));
        }

        [Route("Report/SyndicationPostDetail/{*parameters}", RouteName = "TelerikReportViewer_SyndicationPostDetail")]
        [BaseMembersModelAction("CanViewSocialReports")]
        public ActionResult ReportViewer_SyndicationPostDetail(string parameters = "")
        {
			return reportViewer(ViewBag, "SyndicationPostDetail", parameters, "Post Detail Report");
        }

        [Route("Report/SyndicationPostStream/{*parameters}", RouteName = "TelerikReportViewer_SyndicationPostStream")]
        [BaseMembersModelAction("CanViewSocialReports")]
        public ActionResult ReportViewer_SyndicationPostStream(string parameters = "")
        {
            return reportViewer(ViewBag, "SyndicationPostStream", parameters, "Post Stream Report");
        }

        [Route("Report/PostPerformance/{*parameters}", RouteName = "TelerikReportViewer_PostPerformance")]
        [BaseMembersModelAction("CanViewSyndicatorReports")]
        public ActionResult ReportViewer_PostPerformance(string parameters = "")
        {
            return reportViewer(ViewBag, "PostPerformance", parameters, "Post Performance Report");
        }

		[Route("Report/CompleteDealerReputationReport-MonthlyRatings/{*parameters}", RouteName = "TelerikReportViewer_CompleteDealerReputationReport-MonthlyRatings")]
		[BaseMembersModelAction("CanViewReputationReports")]
		public ActionResult ReportViewer_CompleteDealerReputationReport_MonthlyRatings(string parameters = "")
		{
			return reportViewer(ViewBag, "CompleteDealerReputationReport-MonthlyRatings", parameters, "Reputation Monthly Ratings Report");
		}

		[Route("Report/CompleteDealerReputationReport-MonthlyReviewTotals/{*parameters}", RouteName = "TelerikReportViewer_CompleteDealerReputationReport-MonthlyReviewTotals")]
		[BaseMembersModelAction("CanViewReputationReports")]
		public ActionResult ReportViewer_CompleteDealerReputationReport_MonthlyReviewTotals(string parameters = "")
		{
			return reportViewer(ViewBag, "CompleteDealerReputationReport-MonthlyReviewTotals", parameters, "Reputation Monthly Review Report");
		}

		[Route("Report/CompleteDealerReputationReport-Summary/{*parameters}", RouteName = "TelerikReportViewer_CompleteDealerReputationReport-Summary")]
		[BaseMembersModelAction("CanViewReputationReports")]
		public ActionResult ReportViewer_CompleteDealerReputationReport_Summary(string parameters = "")
		{
			return reportViewer(ViewBag, "CompleteDealerReputationReport-Summary", parameters, "Reputation Summary Report");
		}

		[Route("Report/EnterpriseSocialNetworkReport/{*parameters}", RouteName = "TelerikReportViewer_EnterpriseSocialNetworkReport")]
		[BaseMembersModelAction("CanViewSocialReports")]
		public ActionResult ReportViewer_EnterpriseSocialNetworkReport(string parameters = "")
		{
			return reportViewer(ViewBag, "EnterpriseSocialNetworkReport", parameters, "Enterprise Social Network Report");
		}

		[Route("Report/SocialMediaReport/{*parameters}", RouteName = "TelerikReportViewer_SocialMediaReport")]
		[BaseMembersModelAction("CanViewSocialReports")]
		public ActionResult ReportViewer_SocialMediaReport(string parameters = "")
		{
			return reportViewer(ViewBag, "SocialMediaReport", parameters, "Social Media Report");
		}

        [Route("Report/AdminPublishReport", RouteName = "TelerikReportViewer_AdminPublishReport")]
        [BaseMembersModelAction("CanViewAdminReports")]
        public ActionResult ReportViewer_AdminPublishReport(string parameters = "")
        {
            return reportViewer(ViewBag, "AdminPublishReport", parameters, "Admin Publish Report");
        }

        [Route("Report/SyndicationSignups", RouteName = "TelerikReportViewer_SyndicationSignups")]
        [BaseMembersModelAction("CanViewAdminReports")]
        public ActionResult ReportViewer_SyndicationSignups(string parameters = "")
        {
            return reportViewer(ViewBag, "SyndicationSignups", parameters, "Admin Syndication Signup Report");
        }

		[Route("Report/PostSummary", RouteName = "TelerikReportViewer_PostSummary")]
        [BaseMembersModelAction("CanMenuReportPostSummary")]
		public ActionResult ReportViewer_PostSummary(string parameters = "")
		{
			return reportViewer(ViewBag, "PostSummary", parameters, "Post Summary Report");
		}

        

        [Route("Report/AdminReputationDetails", RouteName = "TelerikReportViewer_AdminReputationDetails")]
        [BaseMembersModelAction("CanViewAdminReports")]
        public ActionResult ReportViewer_AdminReputationDetails(string parameters = "")
        {
            return reportViewer(ViewBag, "AdminReputationDetails", parameters, "Admin Reputation Details Report");
        }

        //filterContext.Controller.ViewBag.BaseModel
        //[Route("Report/{reportFileName}/{*parameters}", RouteName = "TelerikReportViewer")]
        [Route("Report/Admin/{*reportFileName}", RouteName = "TelerikReportViewer_AdminReports")]
        [BaseMembersModelAction("CanViewAdminReports")]
        public ActionResult ReportViewer_AdminReports(string reportFileName, string parameters = "")
        {
            return reportViewer(ViewBag, reportFileName, parameters, "Admin Report");
        }

        private ActionResult reportViewer(dynamic ViewBag, string reportFileName, string parameters = "", string title="")
        {
            BaseMembersModel model = ViewBag.baseModel as BaseMembersModel;

            ViewBag.ReportFileName = reportFileName + ".trdx";
	        ViewBag.Title = title;
            ViewBag.AssetPath = Settings.GetSetting("site.PublishImageUploadURL");

            UriReportSource reportSource = new UriReportSource();
            reportSource.Uri = ViewBag.ReportFileName;

            //default parameters
            reportSource.Parameters.Add("userID", model.UserInfo.EffectiveUser.ID.GetValueOrDefault());
            reportSource.Parameters.Add("contextAccountID", model.UserInfo.EffectiveUser.CurrentContextAccountID);

            TimeZoneInfo timeZoneInfo = ProviderFactory.TimeZones.GetTimezoneForUserID(model.UserInfo.EffectiveUser.ID.GetValueOrDefault());
            TimeSpan utcOffset = timeZoneInfo.BaseUtcOffset;
            if (timeZoneInfo.SupportsDaylightSavingTime && timeZoneInfo.IsDaylightSavingTime(model.UserInfo.EffectiveUser.CurrentDateTime.LocalDate.GetValueOrDefault()))
                utcOffset = utcOffset.Add(new TimeSpan(0, 0, 60, 0));
            reportSource.Parameters.Add("userUTCOffsetMinutes", (int)utcOffset.TotalMinutes);

            if (!string.IsNullOrEmpty(parameters))
            {
                string[] paramItems = parameters.Split('|');
                foreach (string item in paramItems)
                {
                    string[] pairs = item.Split(',');
                    reportSource.Parameters.Add(new Parameter(pairs[0], pairs[1]));
                }
            }

            ViewBag.ReportSource = reportSource;
            return View("ReportViewer");
        }

		public JsonResult ReportAccounts(string dealerFilter)
        {
			BaseMembersModel model = new BaseMembersModel();

			List<SelectListItem> accounts = ReportsService.GetAccounts(model.UserInfo, dealerFilter);

			return Json(accounts, JsonRequestBehavior.AllowGet);
        }

		[Route("Report/ReputationSnapshot", RouteName = "ReputationSnapshotReport")]
		[BaseMembersModelAction]
		public ActionResult ReputationSnapshot()
		{
			List<int> ReportYears = new List<int>();
			for (int year = 2013; year <= DateTime.Now.Year; year++)
			{
				ReportYears.Add(year);
			}
			ViewBag.Years = ReportYears;

			return View();
		}

		public ActionResult ReputationSnapshotHtml(string accountID, DateTime? reportDate, bool? printHtml)
		{
			string html = ProviderFactory.Report.GetReputationSnapshotReportHTML(accountID.ToLong(), reportDate.GetValueOrDefault(DateTime.Now), printHtml.GetValueOrDefault());
			return Content(html);
		}

		[Route("Report/SocialSnapshot", RouteName = "SocialSnapshotReport")]
		[BaseMembersModelAction]
		public ActionResult SocialSnapshot()
		{
			List<int> ReportYears = new List<int>();
			for (int year = 2013; year <= DateTime.Now.Year; year++)
			{
				ReportYears.Add(year);
			}
			ViewBag.Years = ReportYears;

			return View();
		}

		public ActionResult SocialSnapshotHtml(string accountID, DateTime? reportDate, bool? printHtml)
		{
			string html = ProviderFactory.Report.GetSocialSnapshotReportHTML(accountID.ToLong(), reportDate.GetValueOrDefault(DateTime.Now), printHtml.GetValueOrDefault());
			return Content(html);
		}

        [Route("Report/EnterpriseReputationSocial", RouteName = "EnterpriseReputationSocialReport")]
		[BaseMembersModelAction]
        public ActionResult EnterpriseReputationSocial()
		{
			return View();
		}

        public ActionResult EnterpriseReputationSocialHtml(string accountID, bool? printHtml)
        {
            BaseMembersModel model = new BaseMembersModel();
            string html = ProviderFactory.Report.GetReputationAndSocialReportHTML(accountID.ToLong() , model.UserInfo.EffectiveUser.ID.GetValueOrDefault(), printHtml.GetValueOrDefault());
            return Content(html);
        }

		[Route("Report/EnterpriseReputationSocial2", RouteName = "EnterpriseReputationSocialReport2")]
		[BaseMembersModelAction]
		public ActionResult EnterpriseReputationSocialIA()
		{
			return View();
		}

		[HttpPost]
		public JsonResult EnterpriseReputationSocialGrid([DataSourceRequest] DataSourceRequest request, string accountIDs, long? VGID)
		{
			BaseMembersModel model = new BaseMembersModel();

			List<EnterpriseReputationSocial> report = ReportsService.EnterpriseReputationSocialGrid(model.UserInfo, KendoSupport.PagedGridRequest(request), accountIDs, VGID);

			return Json(report.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
		}
		
		[HttpPost]
		public JsonResult EnterpriseReputationSocialDetailGrid([DataSourceRequest] DataSourceRequest request, string accountIDs, long? VGID)
		{
			BaseMembersModel model = new BaseMembersModel();

			List<EnterpriseReputationSocialDetail> report = ReportsService.EnterpriseReputationSocialDetailGrid(model.UserInfo, KendoSupport.PagedGridRequest(request), accountIDs, VGID);

			return Json(report.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult ExportToExcel(string model, string data, string title, string detailModel, string detailData)
		{
			Session[title] = ReportsService.CreateEnterpriseRepSocialExcel(model, data, detailModel, detailData);

			return Json(new { success = true }, JsonRequestBehavior.AllowGet);
		}

		public FileResult GetExcelFile(string title)
		{
			if (Session[title] != null)
			{
				byte[] file = Session[title] as byte[];
				string filename = string.Format("{0}.xlsx", title.StripWhiteSpace().Replace(",", "").Replace("%", ""));
				Session.Remove(title);

				Response.Buffer = true;
				
				return File(file, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", filename);
			}

			return null;
		}

		[HttpPost]
		public JsonResult ExportToPDF(string model, string data, string title)
		{
			Session[title] = ReportsService.CreateEnterpriseRepSocialPdf(model, data);

			return Json(new { success = true }, JsonRequestBehavior.AllowGet);
		}

		public FileResult GetPDFFile(string title)
		{
			if (Session[title] != null)
			{
				byte[] file = Session[title] as byte[];
				string filename = string.Format("{0}.pdf", title.StripWhiteSpace().Replace(",", "").Replace("%", ""));
				Session.Remove(title);

				Response.Buffer = true;

				return File(file, "application/pdf", filename);
			}

			return null;
		}
    }
}
