﻿var Notifications = {
	currentContext: 0,
	approvalsUrl: '',
	notificationLVBound: function (e) {
		//IE 10 hack to cause repaint for box shadow
		if ($('html').hasClass("k-ie10")) {
			if ($(".todo-noti-panel").css("width") == "400px")
				$(".todo-noti-panel").css("width", "401px");
			else
				$(".todo-noti-panel").css("width", "400px");
		}

		kendo.ui.progress($(".todo-noti-panel"), false);
		$("#panelContent").fadeIn("fast");
		$("#AlertCount").html(e.sender.dataSource._data.length);
	},

	toDoBound: function (e) {
		$("#ToDoCount").html(e.sender.dataSource._data.length);
	},

	notificationsChange: function (e) {
		var item = e.sender.select();
		var notif = e.sender.dataSource.getByUid(item.data().uid);
		if (notif.ActionImage == "approve_post.png")
			document.location.href = Notifications.approvalsUrl;
		else {
			$("#filterParameters").val("locations,eq," + Notifications.currentContext + "-status,HasBeenRead,false");
			$("#ReviewStreamForm").submit();
		}
	}
}