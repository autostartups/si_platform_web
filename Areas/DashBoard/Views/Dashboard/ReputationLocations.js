﻿
$(function () {

    $("#activeFilter").click(function () {
    	if ($(".stream-filter").css("display") == "block")
    		$(".stream-filter").slideUp("fast");
    	else
    		$(".stream-filter").slideDown("fast");
    });


});

function filterLocations(e) {
	var filterSel = $("#TimeFrameFilter").val();
	if (filterSel != "Custom") {
		var ds = $("#LocationList").data().kendoGrid.dataSource;
		ds.transport.options.read.url = '/Dashboard/Dashboard/LocationsGrid' + "?timeFrame=" + filterSel;
		ds.read();
	} else {
		$(".stream-filter").slideDown("fast");
		$(".filter-dates").fadeIn();
	}
}


function getLocation(dealerLoc) {
	var templt = kendo.template($("#location-template").html());
	return templt({ DealerLocation: dealerLoc });
}

function locationsDataBound(e) {
	$("#LocationList tbody tr .reputation-filter-links").each(function () {
		var me = this;
		$(me).click(function () {
			var filter = $(this).data("filter");
			$("#filterParameters").val(filter);
			$("#ReviewStreamForm").submit();
		});
	});


	$("#LocationList tbody tr .sites-filter-links").each(function () {
		var me = this;
		$(me).click(function () {
			var filter = $(this).data("filter");
			$("#filterParameters").val(filter);
			$("#ReviewStreamForm").submit(); 
		});
	});
}
    
function DealerLink(id) {
	$("#filterParameters").val("locations,eq," + id);
	$("#ReviewStreamForm").submit();
}
	
function GoToSocialLocation() {
	SIGlobal.showGridErrors = false;
	document.location.href = '/Dashboard/SocialLocations';
	return false;
}


