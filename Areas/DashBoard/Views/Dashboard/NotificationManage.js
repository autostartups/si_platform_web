﻿

$(function() {
    $("#ShowCompletedToDos").click(function() {
    	$("#ToDoNotificationGrid").data("kendoGrid").dataSource.read();
    });

    $("#ClearAll").click(function(e) {
    	e.preventDefault();
    	e.stopPropagation();
    	var url = '/Dashboard/Dashboard/ClearAllAlerts';
    	$.post(url, function(data) {
    		if (data.success) {
    			$("#NotificationMangerGrid").data("kendoGrid").dataSource.read();
    		}
    		SIGlobal.alertDialog("Clear All Notifications", data.message);
    	});
    });
});
	
function showCompleted() {
	return {
		showCompleted: $("#ShowCompletedToDos").is(":checked")
	};
}

function getCompleted(completed, completedDate) {
	var templt = kendo.template($("#completed-template").html());
	return templt({ completed: completed, completedDate: completedDate });
}
	
function notificationChange(e) {
	var item = this.select();
	if (item.find('.ico-todo-add,.ico-alert-clear,.ico-complete').length > 0) {
		item.removeClass('k-state-selected');
		return;
	}
		
	var data = this.dataItem(item.closest('tr'));
	if (data.Status != "Pending Post Approval") {
		$("#filterParameters").val("locations,eq," + data.ID + "-status,HasBeenRead,false");
		$("#ReviewStreamForm").submit();
	} else
		document.location.href = '/Dashboard/Approvals';
}
	
//function toDosBound() {
//	$(".ico-complete").click(function (e) {
//		e.preventDefault();
//		e.stopPropagation();

//		var data = GetRowData('#ToDoNotificationGrid', this);
//		var url = '@Url.Action("CompleteToDo", "Dashboard", new { area = "Dashboard"})';
//		var addData = { notificationID: data.ID };
//		$.post(url, addData, function (data) {
//			if (data.success) {
//				$("#ToDoNotificationGrid").data("kendoGrid").dataSource.read();
//			}
//			SIGlobal.alertDialog("To Dos Complete", data.message);
//		});
//	});
//}

	//function alertsBound() {
	//	$(".ico-todo-add").click(function (e) {
	//		e.preventDefault();
	//		e.stopPropagation();
			
	//		var data = GetRowData('#AlertNotificationGrid', this);
	//		var url = '@Url.Action("AddAlertToToDos", "Dashboard", new { area = "Dashboard"})';
	//		var addData = { notificationID: data.ID };
	//		$.post(url, addData, function (data) {
	//			if (data.success) {
	//				$("#ToDoNotificationGrid").data("kendoGrid").dataSource.read();
	//				$("#AlertNotificationGrid").data("kendoGrid").dataSource.read();
	//			}
	//			SIGlobal.alertDialog("Add Notification to To Dos", data.message);
	//		});
	//	});
	//	$(".ico-alert-clear").click(function (e) {
	//		e.preventDefault();
	//		e.stopPropagation();
			
	//		var data = GetRowData('#AlertNotificationGrid', this);
	//		var url = '@Url.Action("ClearAlert", "Dashboard", new { area = "Dashboard"})';
	//		var addData = { notificationID: data.ID };
	//		$.post(url, addData, function (data) {
	//			if (data.success) {
	//				$("#ToDoNotificationGrid").data("kendoGrid").dataSource.read();
	//				$("#AlertNotificationGrid").data("kendoGrid").dataSource.read();
	//			}
	//			SIGlobal.alertDialog("Clear Notification", data.message);

	//		});
	//	});
	//}
	
	function GetRowData(gridSelector, row) {
		return $(gridSelector).data().kendoGrid.dataSource.getByUid($(row).closest('tr').data("uid"));
	}
