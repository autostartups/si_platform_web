﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SI.WEB.Portal.Models
{
    public class Crumb
    {
        public string Name { get; set; }
        public string URL { get; set; }
    }
}