﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SI.WEB.Portal.Models
{
    public class DialogModel
    {
        public int Width { get; set; }
        public bool Resizable { get; set; }
        public bool Modal { get; set; }
    }
}