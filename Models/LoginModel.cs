﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SI.WEB.Portal.Models
{
	public class LoginModel
	{
		[Required]
		[Display(Name = "User name: ")]
		public string UserName { get; set; }

		[Required]
		[DataType(DataType.Password)]
		[Display(Name = "Password: ")]
		public string Password { get; set; }

		public bool RememberMe { get; set; }

		[Display(Name = "Email Address: ")]
		[StringLength(70)]
		[DataType(DataType.EmailAddress)]
		[RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Email is not valid.")]
		public string EmailAddress { get; set; }
	}
}