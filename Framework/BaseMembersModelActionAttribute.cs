﻿
using System.Web.Mvc;
using SI.BLL;
using SI.WEB.Portal.Models;

namespace SI.WEB.Portal.Framework
{
	public class BaseMembersModelActionAttribute : FilterAttribute, IActionFilter
	{	
		private string _membersPath = @"\";
		private string _permission = string.Empty;

		public BaseMembersModelActionAttribute(string permission = "", string membersPath = "", int order = 2)
		{
			Order = order;
			_membersPath = membersPath;
			_permission = permission;
		}

		public void OnActionExecuting(ActionExecutingContext filterContext)
		{
			BaseMembersModel model = new BaseMembersModel();

			model.MembersPath = _membersPath;
			if (string.IsNullOrEmpty(_membersPath))
				model.MembersPath = string.Join("/", filterContext.RouteData.Values.Values);
			
			filterContext.Controller.ViewBag.BaseModel = model;

			if (!string.IsNullOrEmpty(_permission))
			{
				bool perm = (bool)model.UserInfo.EffectiveUser.GetType().GetProperty(_permission).GetValue(model.UserInfo.EffectiveUser);
				if (!perm)
				{
					filterContext.Result = new RedirectToRouteResult("DashboardOverview", null);
				}
					
			}

		}

		public void OnActionExecuted(ActionExecutedContext filterContext)
		{
			
		}
	}
}