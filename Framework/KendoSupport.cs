﻿using System.ComponentModel;
using System.Linq;
using Kendo.Mvc;
using Kendo.Mvc.UI;
using SI.Services.Framework.Models;



namespace SI.WEB.Portal.Framework
{
	public static class KendoSupport
	{
		public static PagedGridRequest PagedGridRequest(DataSourceRequest request)
		{
			PagedGridRequest pagedGridRequest = new PagedGridRequest();
			pagedGridRequest.Skip = (request.Page - 1) * request.PageSize;
			pagedGridRequest.Take = request.PageSize;

			if (request.Sorts != null)
			{
				foreach (SortDescriptor sort in request.Sorts)
				{
					PagedGridSortDescriptor pagedGridSort = new PagedGridSortDescriptor();
					pagedGridSort.Field = sort.Member;
					if (sort.SortDirection == ListSortDirection.Ascending)
						pagedGridSort.Ascending = true;

					pagedGridRequest.Sorting.Add(pagedGridSort);
				}
			}

			if (request.Filters != null)
			{
				foreach (var filter in request.Filters)
				{
					if (filter is FilterDescriptor)
					{
						FilterDescriptor fd = filter as FilterDescriptor;
						PagedGridFilterDescriptor pagedGridFilter = new PagedGridFilterDescriptor();

						pagedGridFilter.Field = fd.Member;
						pagedGridFilter.Value = fd.Value.ToString();
						pagedGridFilter.Operator = fd.Operator;
						pagedGridFilter.MemberType = fd.ConvertedValue.GetType();

						pagedGridRequest.Filtering.Add(pagedGridFilter);
					}
					else if (filter is CompositeFilterDescriptor)
					{
						CompositeFilterDescriptor cf = filter as CompositeFilterDescriptor;
						if (cf.LogicalOperator == FilterCompositionLogicalOperator.And)
							pagedGridRequest.CompositeAndFilter = true;
						if (cf.LogicalOperator == FilterCompositionLogicalOperator.Or)
							pagedGridRequest.CompositeOrFilter = true;

						ExtractFilters(cf, pagedGridRequest);
					}
				}
			}
			
			return pagedGridRequest;
		}



		private static void ExtractFilters(CompositeFilterDescriptor filterList, PagedGridRequest pagedGridRequest)
		{
			if (filterList == null || filterList.FilterDescriptors == null)
				return;
			if (filterList.FilterDescriptors.Any() == false)
				return;

			foreach (var filter in filterList.FilterDescriptors)
			{
				if (filter is FilterDescriptor)
				{
					FilterDescriptor fd = filter as FilterDescriptor;
					PagedGridFilterDescriptor pagedGridFilter = new PagedGridFilterDescriptor();

					pagedGridFilter.Field = fd.Member;
					pagedGridFilter.Value = fd.Value.ToString();
					pagedGridFilter.Operator = fd.Operator;
					pagedGridFilter.MemberType = fd.ConvertedValue.GetType();

					if (filterList.LogicalOperator == FilterCompositionLogicalOperator.And)
						pagedGridFilter.Logical = "AND";
					else if (filterList.LogicalOperator == FilterCompositionLogicalOperator.Or)
						pagedGridFilter.Logical = "OR";


					pagedGridRequest.Filtering.Add(pagedGridFilter);
				}
				else if (filter is CompositeFilterDescriptor)
				{
					CompositeFilterDescriptor cf = filter as CompositeFilterDescriptor;
					if (cf.LogicalOperator == FilterCompositionLogicalOperator.And)
						pagedGridRequest.CompositeAndFilter = true;
					if (cf.LogicalOperator == FilterCompositionLogicalOperator.Or)
						pagedGridRequest.CompositeOrFilter = true;

					ExtractFilters(cf, pagedGridRequest);
				}
			}
		}
	}


}